package day06

import (
	"reflect"
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func TestReallocateMemory(t *testing.T) {
	tests := []struct {
		in  []int
		out []int
	}{
		{[]int{0, 2, 7, 0}, []int{2, 4, 1, 2}},
		{[]int{2, 4, 1, 2}, []int{3, 1, 2, 3}},
		{[]int{3, 1, 2, 3}, []int{0, 2, 3, 4}},
		{[]int{0, 2, 3, 4}, []int{1, 3, 4, 1}},
		{[]int{1, 3, 4, 1}, []int{2, 4, 1, 2}},
	}

	for _, test := range tests {
		actual := helpers.Copy(test.in)
		debugger := DebuggerArea{actual, nil}
		debugger.reallocateMemory()
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("Debugger{memoryBanks:%v}.reallocateMemory() => %v, want %v", test.in, actual, test.out)
		}
	}
}

func TestCurrentMemoryInHistory(t *testing.T) {
	tests := []struct {
		memory  []int
		history [][]int
		out     bool
	}{
		{[]int{1, 2, 3, 4}, [][]int{{4, 3, 2, 1}}, false},
		{[]int{1, 2, 3, 4}, [][]int{{1, 2, 3, 4}}, true},
		{[]int{2, 4, 1, 2}, [][]int{{0, 2, 7, 0}, {2, 4, 1, 2}, {3, 1, 2, 3}}, true},
		{[]int{0, 4, 1, 2}, [][]int{{0, 2, 7, 0}, {2, 4, 1, 2}, {3, 1, 2, 3}}, false},
	}

	for _, test := range tests {
		debugger := DebuggerArea{test.memory, test.history}
		actual := debugger.currentMemoryInHistory()
		if actual != test.out {
			t.Errorf("Debugger%+v.currentMemoryInHistory() => %t, want %t", debugger, actual, test.out)
		}
	}
}
