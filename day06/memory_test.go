package day06

import (
	"testing"
)

func TestMemoryReallocation(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{0, 2, 7, 0}, 5},
	}

	for _, test := range tests {
		debugger := DebuggerArea{test.in, make([][]int, 0)}
		actual := MemoryReallocation(debugger)
		if actual != test.out {
			t.Errorf("MemoryReallocation(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMemorySeenAgain(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{2, 4, 1, 2}, 4},
	}

	for _, test := range tests {
		debugger := DebuggerArea{test.in, nil}
		actual := MemorySeenAgain(debugger)
		if actual != test.out {
			t.Errorf("MemorySeenAgain(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
