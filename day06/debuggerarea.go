package day06

import (
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type DebuggerArea struct {
	memoryBanks []int
	history     [][]int
}

func New(memory []int) DebuggerArea {
	return DebuggerArea{memory, make([][]int, 0)}
}

func (da *DebuggerArea) reallocateMemory() {
	size := len(da.memoryBanks)

	duplicate := helpers.Copy(da.memoryBanks)
	if da.history != nil {
		da.history = append(da.history, duplicate)
	}

	pos := helpers.MaxIndex(da.memoryBanks)
	items := da.memoryBanks[pos]
	da.memoryBanks[pos] = 0
	for i := 0; i < items; i++ {
		pos = (pos + 1) % size
		da.memoryBanks[pos] += 1
	}
}

func (da *DebuggerArea) currentMemoryInHistory() bool {
	for _, historyItem := range da.history {
		if helpers.Equal(historyItem, da.memoryBanks) {
			return true
		}
	}
	return false
}
