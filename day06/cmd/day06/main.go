package main

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/day06"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	memory := getMemory(input)
	debugger := day06.New(memory)

	fmt.Printf("MemoryReallocation: %d\n", day06.MemoryReallocation(debugger))
	fmt.Printf("MemorySeenAgain: %d\n", day06.MemorySeenAgain(debugger))
}

func getMemory(input string) (memory []int) {
	for _, data := range strings.Fields(input) {
		size, err := strconv.ParseInt(data, 10, 64)
		if err != nil {
			panic(err)
		}
		memory = append(memory, int(size))
	}
	return
}
