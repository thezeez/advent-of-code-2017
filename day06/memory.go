package day06

import (
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func MemoryReallocation(debugger DebuggerArea) (cycles int) {
	for cycles = 0; !debugger.currentMemoryInHistory(); cycles++ {
		debugger.reallocateMemory()
	}
	return
}

func MemorySeenAgain(debugger DebuggerArea) (cycles int) {
	target := helpers.Copy(debugger.memoryBanks)

	debugger.history = nil

	for {
		cycles++
		debugger.reallocateMemory()

		if helpers.Equal(debugger.memoryBanks, target) {
			return
		}
	}
}
