package day17

import (
	"container/ring"
)

func ShortCircuitSpinLock(steps, finalOperation int) int {
	spinLock := ring.New(1)
	spinLock.Value = 0
	for i := 1; i <= finalOperation; i++ {
		elem := &ring.Ring{Value: i}
		spinLock.Move(steps).Link(elem)
		spinLock = elem
	}
	return spinLock.Next().Value.(int)
}

func AngrySpinLock(steps, finalOperation int) int {
	var afterZero int
	var pos int
	steps++
	for i := 1; i <= finalOperation; i++ {
		pos += steps
		// I seem to have misplaced my modulo operator...
		for pos > i {
			pos -= i
		}
		if pos == 1 {
			afterZero = i
		}
	}
	return afterZero
}
