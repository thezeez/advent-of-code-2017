package day17

import "testing"

func TestShortCircuitSpinLock(t *testing.T) {
	tests := []struct {
		steps          int
		finalOperation int
		out            int
	}{
		{3, 2017, 638},
		{3, 1, 0},
		{3, 2, 1},
		{3, 3, 1},
		{3, 9, 5},
		{354, 2017, 2000},
	}

	for _, test := range tests {
		actual := ShortCircuitSpinLock(test.steps, test.finalOperation)
		if actual != test.out {
			t.Errorf("ShortCircuitSpinLock(%d, %d) => %d, want %d", test.steps, test.finalOperation, actual, test.out)
		}
	}
}

func TestAngrySpinLock(t *testing.T) {
	tests := []struct {
		steps          int
		finalOperation int
		out            int
	}{
		{3, 1, 1},
		{3, 2, 2},
		{3, 3, 2},
		{3, 4, 2},
		{3, 5, 5},
		{3, 6, 5},
		{3, 9, 9},
		{354, 50000000, 10242889},
	}

	for _, test := range tests {
		actual := AngrySpinLock(test.steps, test.finalOperation)
		if actual != test.out {
			t.Errorf("AngrySpinLock(%d, %d) => %d, want %d", test.steps, test.finalOperation, actual, test.out)
		}
	}
}

func BenchmarkAngrySpinLock(b *testing.B) {
	for n := 0; n < b.N; n++ {
		AngrySpinLock(354, 50000000)
	}
}
