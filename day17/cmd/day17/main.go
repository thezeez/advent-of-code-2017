package main

import (
	"fmt"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/day17"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	steps := helpers.IntOrPanic(strings.TrimSpace(input))

	fmt.Printf("ShortCircuitSpinLock: %d\n", day17.ShortCircuitSpinLock(steps, 1024))
	fmt.Printf("AngrySpinLock: %d\n", day17.AngrySpinLock(steps, 50000000))
}
