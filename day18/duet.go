// I am not proud of this one
package day18

import (
	"fmt"
	"strconv"
	"strings"
)

type Instruction struct {
	Command string
	X       interface{}
	Y       interface{}
}

func IntOrRegisterValue(in interface{}, registers map[string]int) int {
	switch val := in.(type) {
	case int:
		return val
	case string:
		return registers[val]
	default:
		panic("neither int nor register")
	}
}

func GetInstructions(input string) (instructions []Instruction) {
	for _, line := range strings.Split(input, "\n") {
		line := strings.TrimSpace(line)
		fields := strings.Fields(line)
		if len(fields) < 2 || fields[0] == "" || fields[1] == "" {
			continue
		}
		instruction := Instruction{
			Command: fields[0],
			X:       maybeInt(fields[1]),
		}
		if len(fields) == 3 {
			instruction.Y = maybeInt(fields[2])
		}
		instructions = append(instructions, instruction)
	}
	return
}

func maybeInt(in string) interface{} {
	if val, err := strconv.Atoi(in); err == nil {
		return val
	}
	return in
}

func Duet(instructions []Instruction) int {
	var lastFrequency int
	registers := make(map[string]int)

	for pos := 0; pos >= 0 && pos < len(instructions); pos++ {
		ins := instructions[pos]

		switch ins.Command {
		case "snd":
			lastFrequency = IntOrRegisterValue(ins.X, registers)
		case "set":
			if register, ok := ins.X.(string); ok {
				registers[register] = IntOrRegisterValue(ins.Y, registers)
			}
		case "add":
			if register, ok := ins.X.(string); ok {
				registers[register] += IntOrRegisterValue(ins.Y, registers)
			}
		case "mul":
			if register, ok := ins.X.(string); ok {
				registers[register] *= IntOrRegisterValue(ins.Y, registers)
			}
		case "mod":
			if register, ok := ins.X.(string); ok {
				registers[register] %= IntOrRegisterValue(ins.Y, registers)
			}
		case "rcv":
			if IntOrRegisterValue(ins.X, registers) != 0 {
				return lastFrequency
			}
		case "jgz":
			if IntOrRegisterValue(ins.X, registers) > 0 {
				pos += IntOrRegisterValue(ins.Y, registers) - 1
			}
		}
	}
	return -1
}

func ActualDuet(instructions []Instruction) int {
	registers := []map[string]int{
		make(map[string]int),
		make(map[string]int),
	}
	registers[0]["p"] = 0
	registers[1]["p"] = 1

	positions := []int{0, 0}

	queues := [][]int{{}, {}}

	currentProgram, nextProgram := 0, 1

	countedSends := 0

	for {
		pos, sends, blocked := Program(currentProgram, nextProgram, queues, instructions, registers[currentProgram], positions[currentProgram])
		if currentProgram == 1 {
			countedSends += sends
		}
		positions[currentProgram] = pos
		if blocked && len(queues[nextProgram]) == 0 {
			break
		}
		currentProgram, nextProgram = nextProgram, currentProgram
	}
	return countedSends
}

func Program(from, to int, queues [][]int, instructions []Instruction, registers map[string]int, lastPos int) (pos, sends int, blocked bool) {
	blocked = false

	for pos = lastPos; pos >= 0 && pos < len(instructions); pos++ {
		ins := instructions[pos]

		switch ins.Command {
		case "snd":
			queues[to] = append(queues[to], IntOrRegisterValue(ins.X, registers))
			sends++
		case "set":
			if register, ok := ins.X.(string); ok {
				registers[register] = IntOrRegisterValue(ins.Y, registers)
			}
		case "add":
			if register, ok := ins.X.(string); ok {
				registers[register] += IntOrRegisterValue(ins.Y, registers)
			}
		case "mul":
			if register, ok := ins.X.(string); ok {
				registers[register] *= IntOrRegisterValue(ins.Y, registers)
			}
		case "mod":
			if register, ok := ins.X.(string); ok {
				registers[register] %= IntOrRegisterValue(ins.Y, registers)
			}
		case "rcv":
			if len(queues[from]) == 0 {
				blocked = true
				return
			}
			if register, ok := ins.X.(string); ok {
				registers[register], queues[from] = queues[from][0], queues[from][1:]
			}
		case "jgz":
			if IntOrRegisterValue(ins.X, registers) > 0 {
				pos += IntOrRegisterValue(ins.Y, registers) - 1
			}
		}
	}
	fmt.Println("out of range")
	return
}
