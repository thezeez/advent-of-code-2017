package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day18"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	instructions := day18.GetInstructions(input)

	fmt.Printf("Duet: %d\n", day18.Duet(instructions))
	fmt.Printf("ActualDuet: %d\n", day18.ActualDuet(instructions))
}
