package day18

import "testing"

func TestDuet(t *testing.T) {
	tests := []struct {
		in  []Instruction
		out int
	}{
		{[]Instruction{
			{"set", "a", 1},
			{"add", "a", 2},
			{"mul", "a", "a"},
			{"mod", "a", 5},
			{"snd", "a", nil},
			{"set", "a", 0},
			{"rcv", "a", nil},
			{"jgz", "a", -1},
			{"set", "a", 1},
			{"jgz", "a", -2},
		}, 4},
	}

	for _, test := range tests {
		actual := Duet(test.in)
		if actual != test.out {
			t.Errorf("Duet(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestActualDuet(t *testing.T) {
	tests := []struct {
		in  []Instruction
		out int
	}{
		{[]Instruction{
			{"snd", 1, nil},
			{"snd", 2, nil},
			{"snd", "p", nil},
			{"rcv", "a", nil},
			{"rcv", "b", nil},
			{"rcv", "c", nil},
			{"rcv", "d", nil},
		}, 3},
	}

	for _, test := range tests {
		actual := ActualDuet(test.in)
		if actual != test.out {
			t.Errorf("ActualDuet(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
