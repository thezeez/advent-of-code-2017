package day25

import "testing"

func TestTuringMachine_Run(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`Begin in state A.
Perform a diagnostic checksum after 6 steps.

In state A:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state B.
  If the current value is 1:
    - Write the value 0.
    - Move one slot to the left.
    - Continue with state B.

In state B:
  If the current value is 0:
    - Write the value 1.
    - Move one slot to the left.
    - Continue with state A.
  If the current value is 1:
    - Write the value 1.
    - Move one slot to the right.
    - Continue with state A.`, 3},
	}

	for _, test := range tests {
		machine := NewTuringMachine(test.in)
		actual := machine.Run()
		if actual != test.out {
			t.Errorf("TuringMachine{%q}.Run() => %d, want %d", test.in, actual, test.out)
		}
	}
}
