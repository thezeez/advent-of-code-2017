package day25

import (
	"regexp"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type TuringMachine struct {
	tape     map[int]int
	cursor   int
	states   map[string]State
	state    string
	checksum int
}

func NewTuringMachine(input string) TuringMachine {
	start := regexp.MustCompile(`Begin in state ([A-Z])\.`).FindStringSubmatch(input)[1]
	checks := regexp.MustCompile(`Perform a diagnostic checksum after (\d+) steps\.`).FindStringSubmatch(input)[1]
	states := regexp.MustCompile(`In state ([A-Z]):
  If the current value is (\d):
    - Write the value (\d)\.
    - Move one slot to the (left|right)\.
    - Continue with state ([A-Z])\.
  If the current value is (\d):
    - Write the value (\d)\.
    - Move one slot to the (left|right)\.
    - Continue with state ([A-Z])\.`).FindAllStringSubmatch(input, -1)

	machine := TuringMachine{
		state:    start,
		states:   make(map[string]State),
		tape:     make(map[int]int),
		checksum: helpers.IntOrPanic(checks),
	}
	for _, state := range states {
		machine.states[state[1]] = ParseState(state[2:])
	}
	return machine
}

func (t *TuringMachine) Run() int {
	for i := 0; i < t.checksum; i++ {
		condition := t.states[t.state].conditions[t.tape[t.cursor]]
		t.tape[t.cursor] = condition.newValue
		t.state = condition.nextState
		t.cursor += condition.movement
	}
	return t.Checksum()
}

func (t *TuringMachine) Checksum() (sum int) {
	for _, v := range t.tape {
		if v == 1 {
			sum++
		}
	}
	return
}
