package day25

import "bitbucket.org/thezeez/advent-of-code-2017/helpers"

type State struct {
	conditions map[int]condition
}

type condition struct {
	newValue  int
	movement  int
	nextState string
}

func ParseState(input []string) State {
	return State{
		conditions: map[int]condition{
			helpers.IntOrPanic(input[0]): parseCondition(input[1:4]),
			helpers.IntOrPanic(input[4]): parseCondition(input[5:]),
		},
	}
}

func parseCondition(input []string) condition {
	return condition{
		newValue:  helpers.IntOrPanic(input[0]),
		movement:  parseMovement(input[1]),
		nextState: input[2],
	}
}

func parseMovement(input string) int {
	switch input {
	case "left":
		return -1
	case "right":
		return 1
	default:
		panic("invalid movement")
	}
}
