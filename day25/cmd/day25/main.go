package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day25"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	machine := day25.NewTuringMachine(input)

	fmt.Printf("Checksum: %d\n", machine.Run())
}
