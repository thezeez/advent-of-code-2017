package day19

import "testing"

func TestMaze_Walk(t *testing.T) {
	tests := []struct {
		in    string
		seen  string
		steps int
	}{
		{`     |
     |  +--+
     A  |  C
 F---|----E|--+
     |  |  |  D
     +B-+  +--+
                `, "ABCDEF", 38},
		{`     |
     |  +--+
     |  |  C
 -F--A----E|--+
     |  |  |  D
     +B-+  +--+
                `, "ABCDEAF", 38},
	}

	for i, test := range tests {
		maze := GetMaze(test.in)
		seen, steps := maze.Walk()
		if seen != test.seen || steps != test.steps {
			t.Errorf("Maze(#%d).Walk() => %q, %d want %q, %d", i, seen, steps, test.seen, test.steps)
		}
	}
}
