package day19

type Coord struct {
	x int
	y int
}

func (c Coord) Add(a Coord) Coord {
	return Coord{
		c.x + a.x,
		c.y + a.y,
	}
}

var (
	DOWN  = Coord{0, 1}
	RIGHT = Coord{1, 0}
	LEFT  = Coord{-1, 0}
	UP    = Coord{0, -1}
)
