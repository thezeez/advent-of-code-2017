package day19

import (
	"strings"
)

type Maze struct {
	Map       [][]byte
	Start     Coord
	Position  Coord
	Direction Coord
	Seen      []byte
}

func GetMaze(input string) Maze {
	out := Maze{Direction: DOWN}
	for i, line := range strings.Split(input, "\n") {
		if i == 0 {
			for x, c := range line {
				if c == '|' {
					out.Start = Coord{x, 0}
					out.Position = Coord{x, 0}
				}
			}
		}
		out.Map = append(out.Map, []byte(line))
	}
	return out
}

func (m *Maze) Walk() (string, int) {
	m.Position = m.Start
	steps := 1
	for m.next() {
		steps++
	}
	if m.mapAt(m.Position) == ' ' {
		steps--
	}
	return string(m.Seen), steps
}

func (m *Maze) next() bool {
	switch m.mapAt(m.Position) {
	case '|', '-': // Valid path, keep going
		m.Position = m.Position.Add(m.Direction)
		return true
	case '+': // Corner, check surroundings
		switch m.Direction {
		case UP, DOWN:
			switch m.mapAt(m.Position.Add(LEFT)) {
			case ' ', '|':
			default:
				m.Direction = LEFT
				m.Position = m.Position.Add(LEFT)
				return true
			}
			switch m.mapAt(m.Position.Add(RIGHT)) {
			case ' ', '|':
			default:
				m.Direction = RIGHT
				m.Position = m.Position.Add(RIGHT)
				return true
			}
		case LEFT, RIGHT:
			switch m.mapAt(m.Position.Add(UP)) {
			case ' ', '-':
			default:
				m.Direction = UP
				m.Position = m.Position.Add(UP)
				return true
			}
			switch m.mapAt(m.Position.Add(DOWN)) {
			case ' ', '-':
			default:
				m.Direction = DOWN
				m.Position = m.Position.Add(DOWN)
				return true
			}
		}
	case ' ': // Invalid position, stop
		return false
	default: // Record non-path position
		m.Seen = append(m.Seen, m.mapAt(m.Position))
		m.Position = m.Position.Add(m.Direction)
		return true
	}
	return false
}

func (m *Maze) mapAt(pos Coord) byte {
	if pos.y < 0 || pos.y >= len(m.Map) {
		return ' '
	}
	if pos.x < 0 || pos.x >= len(m.Map[pos.y]) {
		return ' '
	}
	return m.Map[pos.y][pos.x]
}
