package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day19"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	maze := day19.GetMaze(input)

	seen, steps := maze.Walk()

	fmt.Printf("Seen: %q\n", seen)
	fmt.Printf("Steps: %d\n", steps)
}
