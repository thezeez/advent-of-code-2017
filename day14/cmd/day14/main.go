package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day14"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	grid := day14.New(input)

	fmt.Printf("UsedBits: %d\n", day14.UsedBits(grid))
	fmt.Printf("Regions: %d\n", day14.Regions(grid))
}
