package day14

import "testing"

func TestNew(t *testing.T) {
	tests := []struct {
		in  string
		out [8][8]bool
	}{
		{
			"flqrgnkx",
			[8][8]bool{
				{true, true, false, true, false, true, false, false},
				{false, true, false, true, false, true, false, true},
				{false, false, false, false, true, false, true, false},
				{true, false, true, false, true, true, false, true},
				{false, true, true, false, true, false, false, false},
				{true, true, false, false, true, false, false, true},
				{false, true, false, false, false, true, false, false},
				{true, true, false, true, false, true, true, false},
			},
		},
	}

	for _, test := range tests {
		grid := New(test.in)
		failed := false
		var gridRepresentation string

		for row := 0; row < 8; row++ {
			for col := 0; col < 8; col++ {
				if grid[row][col] != test.out[row][col] {
					failed = true
					gridRepresentation += "X"
				} else {
					if grid[row][col] {
						gridRepresentation += "#"
					} else {
						gridRepresentation += "."
					}
				}
			}
			gridRepresentation += "\n"
		}
		if failed {
			t.Errorf("New(%q) =>\n%s", test.in, gridRepresentation)
		}
	}
}

func TestUsedBits(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"flqrgnkx", 8108},
	}

	for _, test := range tests {
		grid := New(test.in)
		actual := UsedBits(grid)
		if actual != test.out {
			t.Errorf("UsedBits(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRegions(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"flqrgnkx", 1242},
	}

	for _, test := range tests {
		grid := New(test.in)
		actual := Regions(grid)
		if actual != test.out {
			t.Errorf("Regions(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
