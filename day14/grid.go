package day14

import (
	"fmt"

	"encoding/hex"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/day10"
)

const (
	GridRows = 128
	GridCols = 128
)

func UsedBits(grid Grid) (used int) {
	for _, row := range grid {
		for _, col := range row {
			if col {
				used++
			}
		}
	}
	return
}

func Regions(grid Grid) (regions int) {
	for row := 0; row < GridRows; row++ {
		for col := 0; col < GridCols; col++ {
			if grid[row][col] {
				regions++
				grid.EliminateNeighbors(row, col)
			}
		}
	}
	return
}

type Grid [GridRows][GridCols]bool

func (grid *Grid) EliminateNeighbors(row, col int) {
	if row < 0 || row >= GridRows || col < 0 || col >= GridCols {
		return
	}
	if grid[row][col] {
		grid[row][col] = false
		grid.EliminateNeighbors(row-1, col)
		grid.EliminateNeighbors(row+1, col)
		grid.EliminateNeighbors(row, col-1)
		grid.EliminateNeighbors(row, col+1)
	}
}

func New(input string) (grid Grid) {
	input = strings.TrimSpace(input)
	for row := 0; row < GridRows; row++ {
		hash := day10.MultiRoundKnotHash(fmt.Sprintf("%s-%d", input, row))
		decoded, err := hex.DecodeString(hash)
		if err != nil {
			panic(err)
		}
		col := 0
		for _, c := range decoded {
			for _, bit := range fmt.Sprintf("%08b", c) {
				grid[row][col] = bit == '1'
				col++
			}
		}
	}
	return
}
