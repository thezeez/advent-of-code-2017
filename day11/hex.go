package day11

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

// Great resource: https://www.redblobgames.com/grids/hexagons/

type Hex struct {
	X int
	Y int
	Z int
}

func (h *Hex) Move(d string) {
	switch d {
	case "n":
		h.Y += 1
		h.Z -= 1
		break
	case "ne":
		h.X += 1
		h.Z -= 1
		break
	case "se":
		h.X += 1
		h.Y -= 1
		break
	case "s":
		h.Y -= 1
		h.Z += 1
		break
	case "sw":
		h.X -= 1
		h.Z += 1
		break
	case "nw":
		h.X -= 1
		h.Y += 1
		break
	default:
		panic(fmt.Errorf("hex: invalid direction: %q", d))
	}
}

func (h *Hex) CenterDistance() int {
	return (helpers.Abs(h.X) + helpers.Abs(h.Y) + helpers.Abs(h.Z)) / 2
}
