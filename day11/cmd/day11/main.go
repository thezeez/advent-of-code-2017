package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day11"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	current, max := day11.GridDistance(input)

	fmt.Printf("CenterDistance: %d\n", current)
	fmt.Printf("Furthest: %d\n", max)
}
