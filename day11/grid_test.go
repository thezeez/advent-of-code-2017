package day11

import "testing"

func TestGridDistance(t *testing.T) {
	tests := []struct {
		in       string
		distance int
		max      int
	}{
		{"ne,ne,ne", 3, 3},
		{"ne,ne,sw,sw", 0, 2},
		{"ne,ne,s,s", 2, 2},
		{"se,sw,se,sw,sw", 3, 3},
	}

	for _, test := range tests {
		actualDistance, actualMax := GridDistance(test.in)
		if actualDistance != test.distance || actualMax != test.max {
			t.Errorf("GridDistance(%q) => %d, %d, want %d, %d", test.in, actualDistance, actualMax, test.distance, test.max)
		}
	}
}
