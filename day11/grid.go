package day11

import (
	"strings"
)

func GridDistance(input string) (current, max int) {
	var grid Hex
	for i, direction := range strings.Split(input, ",") {
		grid.Move(strings.TrimSpace(direction))
		current = grid.CenterDistance()
		if i == 0 || current > max {
			max = current
		}
	}
	return
}
