package day11

import "testing"

func TestHex_CenterDistance(t *testing.T) {
	tests := []struct {
		x int
		y int
		z int

		out int
	}{
		{0, 0, 0, 0},
		{5, -4, -1, 5},
		{5, 0, -5, 5},
	}

	for _, test := range tests {
		hex := Hex{test.x, test.y, test.z}
		actual := hex.CenterDistance()
		if actual != test.out {
			t.Errorf("%#v.CenterDistance() => %d, want %d", hex, actual, test.out)
		}
	}
}
