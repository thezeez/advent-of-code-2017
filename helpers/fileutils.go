package helpers

import (
	"fmt"
	"io/ioutil"
	"os"
)

// ReadStringFromFile reads the file named by filename and returns its contents as a string.
func ReadStringFromFile(filename string) string {
	inputBytes, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening input file: %s", err)
		os.Exit(66)
	}
	return string(inputBytes)
}
