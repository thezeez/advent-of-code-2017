package helpers

import "testing"

var sortStringTests = []struct {
	in  string
	out string
}{
	{"abcdegf", "abcdefg"},
	{"gfedcba", "abcdefg"},
	{"abcdefgfedcba", "aabbccddeeffg"},
}

func TestSortString(t *testing.T) {
	for _, data := range sortStringTests {
		actual := SortString(data.in)
		if actual != data.out {
			t.Errorf("SortString(%q) => %q, want %q", data.in, actual, data.out)
		}
	}
}
