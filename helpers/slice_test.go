package helpers

import (
	"testing"
)

var indexTests = []struct {
	slice   []int
	lookups []indexTestsLookups
}{
	{
		[]int{16, 4, 1, 2, 8}, []indexTestsLookups{
			{16, 0},
			{4, 1},
			{1, 2},
			{2, 3},
			{8, 4},
			{32, -1},
		}},
}

type indexTestsLookups struct {
	value int
	index int
}

func TestIndex(t *testing.T) {
	for _, data := range indexTests {
		for _, lookups := range data.lookups {
			actual := Index(data.slice, lookups.value)
			if actual != lookups.index {
				t.Errorf("Index(%v, %d) => %d, want %d", data.slice, lookups.value, actual, lookups.index)
			}
		}
	}
}

var deleteTests = []struct {
	in      []int
	element int
	out     []int
}{
	{[]int{1, 2, 3}, 2, []int{1, 3}},
	{[]int{1}, 1, []int{}},
	{[]int{1, 2, 3}, 1, []int{2, 3}},
	{[]int{4, 5, 6}, 1, []int{4, 5, 6}},
}

func TestDelete(t *testing.T) {
	for _, data := range deleteTests {
		actual := Delete(data.in, data.element)
		if !Equal(actual, data.out) {
			t.Errorf("Delete(%v, %d) => %v, want %v", data.in, data.element, actual, data.out)
		}
	}
}

var maxIndexTests = []struct {
	in  []int
	out int
}{
	{[]int{1, 2, 3, 4}, 3},
	{[]int{4, 3, 2, 1}, 0},
	{[]int{0, 0, 0, 0}, 0},
}

func TestMaxIndex(t *testing.T) {
	for _, data := range maxIndexTests {
		actual := MaxIndex(data.in)
		if actual != data.out {
			t.Errorf("MaxIndex(%v) => %d, want %d", data.in, actual, data.out)
		}
	}
}

var equalTests = []struct {
	a   []int
	b   []int
	out bool
}{
	{[]int{1, 2, 3, 4}, []int{1, 2, 3, 4}, true},
	{[]int{4, 3, 2, 1}, []int{1, 2, 3, 4}, false},
	{[]int{1, 2, 3, 4}, []int{1, 2, 3}, false},
	{[]int{4, 3, 2}, []int{4, 3, 2, 1}, false},
}

func TestEqual(t *testing.T) {
	a := []int{1, 2, 3, 4}
	b := a
	actual := Equal(a, b)
	if actual != true {
		t.Errorf("Equal(%p, %p) => %t, want %t", a, b, actual, true)
	}

	for _, data := range equalTests {
		actual := Equal(data.a, data.b)
		if actual != data.out {
			t.Errorf("Equal(%v, %v) => %t, want %t", data.a, data.b, actual, data.out)
		}
	}
}
