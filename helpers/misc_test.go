package helpers

import "testing"

var absTests = []struct {
	in  int
	out int
}{
	{1, 1},
	{0, 0},
	{-1, 1},
	{1234, 1234},
	{-4321, 4321},
}

func TestAbs(t *testing.T) {
	for _, data := range absTests {
		actual := Abs(data.in)
		if actual != data.out {
			t.Errorf("Abs(%d) => %d, want %d", data.in, actual, data.out)
		}
	}
}
