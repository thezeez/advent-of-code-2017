package helpers

import (
	"sort"
)

type ByteSlice []byte

func (b ByteSlice) Less(i, j int) bool {
	return b[i] < b[j]
}

func (b ByteSlice) Swap(i, j int) {
	b[j], b[i] = b[i], b[j]
}

func (b ByteSlice) Len() int {
	return len(b)
}

func SortString(input string) string {
	sorted := ByteSlice([]byte(input))
	sort.Sort(sorted)
	return string(sorted)
}
