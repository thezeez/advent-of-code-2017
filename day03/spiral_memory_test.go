package day03

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type coordinates struct {
	x int
	y int
}

func TestDistance(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{1, 0},
		{12, 3},
		{23, 2},
		{1024, 31},
	}

	for _, test := range tests {
		actual := Distance(test.in)
		if actual != test.out {
			t.Errorf("Distance(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestStressTestChan(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{1, 2},
		{2, 4},
		{4, 5},
		{5, 10},
		{10, 11},
		{11, 23},
		{747, 806},
	}

	for _, test := range tests {
		actual := StressTestChan(test.in)
		if actual != test.out {
			t.Errorf("StressTestChan(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestStressTestLoop(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{1, 2},
		{2, 4},
		{4, 5},
		{5, 10},
		{10, 11},
		{11, 23},
		{747, 806},
	}

	for _, test := range tests {
		actual := StressTestLoop(test.in)
		if actual != test.out {
			t.Errorf("StressTestLoop(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestGetSquareValueChan(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{1, 1},
		{2, 1},
		{3, 2},
		{4, 4},
		{5, 5},
		{10, 26},
		{15, 133},
		{16, 142},
	}

	for _, test := range tests {
		actual := getSquareValueChan(test.in)
		if actual != test.out {
			t.Errorf("getSquareValueChan(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestGetSquareValueLoop(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{1, 1},
		{2, 1},
		{3, 2},
		{4, 4},
		{5, 5},
		{10, 26},
		{15, 133},
		{16, 142},
	}

	for _, test := range tests {
		actual := getSquareValueLoop(test.in)
		if actual != test.out {
			t.Errorf("getSquareValueLoop(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestGetNumberSpiralPosition(t *testing.T) {
	tests := []struct {
		in  int
		pos coordinates
	}{
		{1, coordinates{0, 0}},
		{2, coordinates{1, 0}},
		{3, coordinates{1, 1}},
		{4, coordinates{0, 1}},
		{5, coordinates{-1, 1}},
		{6, coordinates{-1, 0}},
		{7, coordinates{-1, -1}},
		{8, coordinates{0, -1}},
		{9, coordinates{1, -1}},
		{10, coordinates{2, -1}},
		{25, coordinates{2, -2}},
	}

	for _, test := range tests {
		actualX, actualY := getNumberSpiralPosition(test.in)
		if actualX != test.pos.x || actualY != test.pos.y {
			t.Errorf("getNumberSpiralPosition(%d) => %d,%d, want %d,%d", test.in, actualX, actualY, test.pos.x, test.pos.y)
		}
	}
}

func TestFindNeighbors(t *testing.T) {
	tests := []struct {
		in  coordinates
		out []int
	}{
		{coordinates{0, 0}, []int{3, 2, 9, 4, 8, 5, 6, 7}},
		{coordinates{1, 0}, []int{12, 11, 10, 3, 9, 4, 1, 8}},
		{coordinates{1, 1}, []int{13, 12, 11, 14, 2, 15, 4, 1}},
		{coordinates{-1, -1}, []int{1, 8, 23, 6, 22, 19, 20, 21}},
	}

	for _, test := range tests {
		missing := helpers.Copy(test.out)
		for neighbor := range findNeighbors(test.in.x, test.in.y) {
			if !helpers.Contains(test.out, neighbor) {
				t.Errorf("findNeighbors(%d, %d) contained %d, wanted one of %v", test.in.x, test.in.y, neighbor, test.out)
			} else {
				missing = helpers.Delete(missing, neighbor)
			}
		}
		if len(missing) > 0 {
			t.Errorf("findNeighbors(%d, %d) missed %v out of %v", test.in.x, test.in.y, missing, test.out)
		}
	}
}

type fn func(int) int

func benchmarkFunc(input int, f fn, b *testing.B) {
	for n := 0; n < b.N; n++ {
		f(input)
	}
}

func BenchmarkStressTestChan1(b *testing.B)      { benchmarkFunc(1, StressTestChan, b) }
func BenchmarkStressTestChan2(b *testing.B)      { benchmarkFunc(2, StressTestChan, b) }
func BenchmarkStressTestChan4(b *testing.B)      { benchmarkFunc(4, StressTestChan, b) }
func BenchmarkStressTestChan4096(b *testing.B)   { benchmarkFunc(4096, StressTestChan, b) }
func BenchmarkStressTestChan325489(b *testing.B) { benchmarkFunc(325489, StressTestChan, b) }

func BenchmarkStressTestLoop1(b *testing.B)      { benchmarkFunc(1, StressTestLoop, b) }
func BenchmarkStressTestLoop2(b *testing.B)      { benchmarkFunc(2, StressTestLoop, b) }
func BenchmarkStressTestLoop4(b *testing.B)      { benchmarkFunc(4, StressTestLoop, b) }
func BenchmarkStressTestLoop4096(b *testing.B)   { benchmarkFunc(4096, StressTestLoop, b) }
func BenchmarkStressTestLoop325489(b *testing.B) { benchmarkFunc(325489, StressTestLoop, b) }
