package day03

import (
	"math"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func Distance(square int) (distance int) {
	x, y := getNumberSpiralPosition(square)
	return helpers.Abs(x) + helpers.Abs(y)
}

func StressTestChan(input int) (value int) {
	for i := 1; value <= input; i++ {
		value = getSquareValueChan(i)
	}
	return
}

func StressTestLoop(input int) (value int) {
	for i := 1; value <= input; i++ {
		value = getSquareValueLoop(i)
	}
	return
}

func getNumberSpiralPosition(square int) (x, y int) {
	// https://math.stackexchange.com/a/163101
	k := int(math.Ceil((math.Sqrt(float64(square)) - 1) / 2))
	t := 2*k + 1
	m := t * t
	t -= 1
	if square >= (m - t) {
		return k - (m - square), -k
	} else {
		m -= t
	}
	if square >= (m - t) {
		return -k, -k + (m - square)

	} else {
		m -= t
	}
	if square >= (m - t) {
		return -k + (m - square), k
	} else {
		return k, k - (m - square - t)
	}
}

func getNumberSpiralIndex(x, y int) int {
	// https://math.stackexchange.com/q/888361
	if helpers.Abs(x) > helpers.Abs(y) {
		if x > 0 {
			return 4*x*x - 3*x + y + 1
		} else {
			return 4*x*x - x - y + 1
		}
	} else {
		if y > 0 {
			return 4*y*y - y - x + 1
		} else {
			return 4*y*y - 3*y + x + 1
		}
	}
}

func findNeighbors(x, y int) <-chan int {
	out := make(chan int)
	go func() {
		for i := -1; i <= 1; i++ {
			for j := -1; j <= 1; j++ {
				if i == 0 && j == 0 {
					continue
				}
				out <- getNumberSpiralIndex(x+i, y+j)
			}
		}
		close(out)
	}()
	return out
}

func getSquareValueChan(position int) (value int) {
	if position == 1 {
		return 1
	}
	x, y := getNumberSpiralPosition(position)
	for neighbor := range findNeighbors(x, y) {
		if neighbor < position {
			neighborValue := getSquareValueChan(neighbor)
			value += neighborValue
		}
	}
	return
}

func getSquareValueLoop(position int) (value int) {
	if position == 1 {
		return 1
	}
	numbers := []int{1}
	for i := 2; i <= position; i++ {
		value = 0
		x, y := getNumberSpiralPosition(i)
		for neighbor := range findNeighbors(x, y) {
			if neighbor < i {
				value += numbers[neighbor-1]
			}
		}
		numbers = append(numbers, value)
	}
	return
}
