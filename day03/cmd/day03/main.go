package main

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/day03"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	puzzle, err := strconv.ParseInt(strings.TrimSpace(input), 10, 64)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Distance: %d\n", day03.Distance(int(puzzle)))
	fmt.Printf("StressTest: %d\n", day03.StressTestLoop(int(puzzle)))
}
