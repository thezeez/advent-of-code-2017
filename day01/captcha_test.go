package day01

import (
	"testing"
)

func TestCaptcha(t *testing.T) {
	tests := []struct {
		in       string
		distance int
		out      int
	}{
		{"1122", 1, 3},
		{"1111", 1, 4},
		{"1234", 1, 0},
		{"91212129", 1, 9},
		{"1212", 2, 6},
		{"1221", 2, 0},
		{"123425", 3, 4},
		{"123123", 3, 12},
		{"12131415", 4, 4}}

	for _, test := range tests {
		actual := Captcha(test.in, test.distance)
		if actual != test.out {
			t.Errorf("Captcha(%q, %d) => %d, want %d", test.in, test.distance, actual, test.out)
		}
	}
}
