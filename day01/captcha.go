package day01

func Captcha(input string, distance int) (output int) {
	length := len(input)
	if length < 2 {
		return
	}
	for i := 0; i < length; i++ {
		output += addUp(input[i], input[(i+distance)%length])
	}
	return
}

func addUp(a, b uint8) (result int) {
	if a == b {
		result = int(a - '0')
	}
	return
}
