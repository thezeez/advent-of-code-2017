package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day01"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	fmt.Printf("Next Digit Captcha: %d\n", day01.Captcha(input, 1))
	fmt.Printf("Halfway Around Captcha: %d\n", day01.Captcha(input, len(input)/2))
}
