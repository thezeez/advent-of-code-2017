package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day08"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	max, highest := day08.RegisterInstructions(input)
	fmt.Printf("Max result: %d\n", max)
	fmt.Printf("Highest: %d\n", highest)
}
