package day08

import (
	"reflect"
	"testing"
)

func TestInstruction_Fill(t *testing.T) {
	tests := []struct {
		in  string
		out Instruction
	}{
		{
			"b inc 5 if a > 1",
			Instruction{
				"b",
				"inc",
				5,
				"a",
				">",
				1,
			},
		},
		{
			"a inc 1 if b < 5",
			Instruction{
				"a",
				"inc",
				1,
				"b",
				"<",
				5,
			},
		},
		{
			"c dec -10 if a >= 1",
			Instruction{
				"c",
				"dec",
				-10,
				"a",
				">=",
				1,
			},
		},
		{
			"c inc -20 if c == 10",
			Instruction{
				"c",
				"inc",
				-20,
				"c",
				"==",
				10,
			},
		},
	}

	for _, test := range tests {
		actual := NewInstruction(test.in)
		if *actual != test.out {
			t.Errorf("NewInstruction(%q) => %+v, want %+v", test.in, actual, test.out)
		}
	}
}

func TestInstruction_Do(t *testing.T) {
	var tests = []struct {
		in    string
		state map[string]int
		out   map[string]int
	}{
		{"b inc 5 if a > 1", map[string]int{}, map[string]int{}},
		{"a inc 1 if b < 5", map[string]int{}, map[string]int{"a": 1}},
		{"c dec -10 if a >= 1", map[string]int{"a": 1}, map[string]int{"a": 1, "c": 10}},
		{"c inc -20 if c == 10", map[string]int{"a": 1, "c": 10}, map[string]int{"a": 1, "c": -10}},
		{"b inc 10 if a <= 1", map[string]int{"a": 1, "c": 10}, map[string]int{"a": 1, "b": 10, "c": 10}},
		{"b dec 20 if c != 5", map[string]int{"a": 1, "b": 10, "c": 10}, map[string]int{"a": 1, "b": -10, "c": 10}},
	}

	for _, test := range tests {
		originalState := copyMap(test.state)
		NewInstruction(test.in).Do(test.state)
		if !reflect.DeepEqual(test.state, test.out) {
			t.Errorf("NewInstruction(%q).Do(%+v) => %+v, want %+v", test.in, originalState, test.state, test.out)
		}
	}
}

func copyMap(in map[string]int) map[string]int {
	out := make(map[string]int)
	for k, v := range in {
		out[k] = v
	}
	return out
}
