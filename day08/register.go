package day08

import (
	"strings"
)

func RegisterInstructions(input string) (int, int) {
	registers := make(map[string]int)
	var highestValueEver int

	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		instruction := NewInstruction(line)

		instruction.Do(registers)

		if registers[instruction.target] > highestValueEver {
			highestValueEver = registers[instruction.target]
		}
	}

	max := -int(^uint(0)>>1) - 1
	for _, value := range registers {
		if value > max {
			max = value
		}
	}
	return max, highestValueEver
}
