package day08

import "testing"

func TestRegisterInstructions(t *testing.T) {
	tests := []struct {
		in      string
		max     int
		highest int
	}{
		{
			`
b inc 5 if a > 1
a inc 1 if b < 5
c dec -10 if a >= 1
c inc -20 if c == 10`,
			1,
			10,
		},
	}

	for _, test := range tests {
		actualMax, actualHighest := RegisterInstructions(test.in)
		if actualMax != test.max || actualHighest != test.highest {
			t.Errorf("RegisterInstructions(%q) => %d, %d, want %d, %d", test.in, actualMax, actualHighest, test.max, test.highest)
		}
	}
}
