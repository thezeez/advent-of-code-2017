package day08

import (
	"fmt"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Instruction struct {
	target           string
	operation        string
	value            int
	comparisonTarget string
	condition        string
	comparisonValue  int
}

func NewInstruction(input string) *Instruction {
	fields := strings.Fields(input)
	if len(fields) != 7 {
		panic(fmt.Errorf("invalid length: %q", fields))
	}
	return &Instruction{
		fields[0],
		fields[1],
		helpers.IntOrPanic(fields[2]),
		fields[4],
		fields[5],
		helpers.IntOrPanic(fields[6]),
	}
}

func (i *Instruction) Do(registers map[string]int) {
	if !condition(registers[i.comparisonTarget], i.condition, i.comparisonValue) {
		return
	}
	switch i.operation {
	case "inc":
		registers[i.target] += i.value
		return
	case "dec":
		registers[i.target] -= i.value
		return
	default:
		panic(fmt.Errorf("instruction: unknown operation: %q", i.operation))
	}
}

func condition(target int, operation string, comparedTo int) bool {
	switch operation {
	case ">":
		return target > comparedTo
	case "<":
		return target < comparedTo
	case ">=":
		return target >= comparedTo
	case "<=":
		return target <= comparedTo
	case "==":
		return target == comparedTo
	case "!=":
		return target != comparedTo
	default:
		panic(fmt.Errorf("invalid operation: %q", operation))
	}
}
