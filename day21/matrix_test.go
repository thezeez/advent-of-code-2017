package day21

import "testing"

func TestNewMatrix(t *testing.T) {
	tests := []struct {
		in  string
		out Matrix
	}{
		{"../.#",
			Matrix{
				{false, false},
				{false, true}}},
		{".#./..#/###",
			Matrix{
				{false, true, false},
				{false, false, true},
				{true, true, true}}},
		{"#..#/..../#..#/.##.",
			Matrix{
				{true, false, false, true},
				{false, false, false, false},
				{true, false, false, true},
				{false, true, true, false}}},
	}
	for _, test := range tests {
		actual := NewMatrix(test.in)
		if !test.out.Equal(actual) {
			t.Errorf("NewMatrix(%q) =>\n%s\nwant\n%s", test.in, actual, test.out)
		}
	}
}

func TestMatrix_GetMatrixAt(t *testing.T) {
	tests := []struct {
		in   Matrix
		top  int
		left int
		size int
		out  Matrix
	}{
		{
			Matrix{
				{true, true, true},
				{true, false, true},
				{true, false, false},
			},
			1, 1, 2,
			Matrix{
				{false, true},
				{false, false},
			},
		},
	}

	for _, test := range tests {
		actual := test.in.GetMatrixAt(test.top, test.left, test.size)
		if !actual.Equal(test.out) {
			t.Errorf("Matrix\n%s\nGetMatrixAt(%d, %d, %d) =>\n%s\nwant\n%s", test.in, test.top, test.left, test.size, actual, test.out)
		}
	}
}

func TestMatrix_Enhance(t *testing.T) {
	tests := []struct {
		in           string
		enhancements string
		out          string
	}{
		{".#./..#/###", "../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#", "#..#/..../..../#..#"},
	}

	for _, test := range tests {
		matrix := NewMatrix(test.in)
		enhancements := GetEnhancements(test.enhancements)
		out := NewMatrix(test.out)
		actual := matrix.Enhance(enhancements)
		if !actual.Equal(out) {
			t.Errorf("Matrix\n%s\nEnhance() =>\n%s\nwant\n%s", matrix, actual, out)
		}
	}
}
