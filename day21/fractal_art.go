package day21

func OnAfterEnhancements(input Matrix, enhancements map[int][]Enhancement, count int) (on int) {
	matrix := input.Copy()
	for i := 0; i < count; i++ {
		matrix = matrix.Enhance(enhancements)
	}
	for _, row := range matrix {
		for _, col := range row {
			if col {
				on++
			}
		}
	}
	return
}
