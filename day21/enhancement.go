package day21

import (
	"strings"
)

type Enhancement struct {
	Size   int
	Inputs []Matrix
	Output Matrix
}

func GetEnhancements(input string) (enhancements map[int][]Enhancement) {
	enhancements = make(map[int][]Enhancement)
	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		enhancement := GetEnhancement(line)
		enhancements[enhancement.Size] = append(enhancements[enhancement.Size], enhancement)
	}
	return
}

func GetEnhancement(input string) Enhancement {
	parts := strings.Split(input, "=>")

	e := Enhancement{}
	inputMatrix := NewMatrix(parts[0])
	var matrices []Matrix
	for i := 0; i < 4; i++ {
		inputMatrix = inputMatrix.Rotate()
		flipped := inputMatrix.Flip()
		var foundInput, foundFlipped bool
		for _, m := range matrices {
			if m.Equal(inputMatrix) {
				foundInput = true
			}
			if m.Equal(flipped) {
				foundFlipped = true
			}
		}
		if !foundInput {
			matrices = append(matrices, inputMatrix)
		}
		if !foundFlipped {
			matrices = append(matrices, flipped)
		}
	}
	e.Inputs = matrices
	e.Size = len(inputMatrix)
	e.Output = NewMatrix(parts[1])
	return e
}

func (e *Enhancement) String() string {
	return e.Inputs[0].String()
}

func (e *Enhancement) Has(m Matrix) bool {
	for i := 0; i < len(e.Inputs); i++ {
		if m.Equal(e.Inputs[i]) {
			return true
		}
	}
	return false
}
