package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day21"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	pattern := day21.NewMatrix(".#./..#/###")
	enhancements := day21.GetEnhancements(input)

	fmt.Printf("On ater 5 enhancements: %d\n", day21.OnAfterEnhancements(pattern, enhancements, 5))
	fmt.Printf("On after 18 enhancements: %d\n", day21.OnAfterEnhancements(pattern, enhancements, 18))
}
