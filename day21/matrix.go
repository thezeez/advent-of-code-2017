package day21

import (
	"strings"
)

type Matrix [][]bool

func NewMatrix(input string) (out Matrix) {
	input = strings.TrimSpace(input)
	for _, line := range strings.Split(input, "/") {
		var l []bool
		for _, c := range line {
			l = append(l, c == '#')
		}
		out = append(out, l)
	}
	return
}

func (m Matrix) Rotate() Matrix {
	length := len(m)
	out := m.Copy()
	for row := 0; row < length/2; row++ {
		for col := row; col < length-row-1; col++ {
			tmp := m[row][col]
			for i := 0; i < 4; i++ {
				tmp, out[col][length-row-1] = m[col][length-row-1], tmp
				row, col = col, length-row-1
			}
		}
	}
	return out
}

func (m Matrix) Flip() Matrix {
	length := len(m)
	out := m.Copy()
	for row := 0; row < length/2; row++ {
		for col := 0; col < length; col++ {
			out[row][col] = m[length-row-1][col]
			out[length-row-1][col] = m[row][col]
		}
	}
	return out
}

func (m Matrix) Copy() Matrix {
	var out Matrix
	for _, row := range m {
		r := append([]bool{}, row...)
		out = append(out, r)
	}
	return out
}

func (m Matrix) String() string {
	var out string
	for i, line := range m {
		for _, row := range line {
			if row {
				out += "#"
			} else {
				out += "."
			}
		}
		if i < len(m)-1 {
			out += "/"
		}
	}
	return out
}

func (m Matrix) Enhance(enhancements map[int][]Enhancement) Matrix {
	subMatrices := m.GetSubMatrices()
	var enhanced [][]Matrix
	for _, row := range subMatrices {
		var l []Matrix
	matrix:
		for _, matrix := range row {
			size := len(matrix)
			for _, enhancement := range enhancements[size] {
				if enhancement.Has(matrix) {
					l = append(l, enhancement.Output)
					continue matrix
				}
			}
			panic("no enhancement found")
		}
		enhanced = append(enhanced, l)
	}

	outerLength := len(enhanced)
	innerLength := len(enhanced[0][0])
	out := NewEmptyMatrix(outerLength * innerLength)

	for outerRow := 0; outerRow < len(enhanced); outerRow++ {
		if len(enhanced[outerRow]) != outerLength {
			panic("invalid outer column length")
		}
		for outerColumn := 0; outerColumn < len(enhanced[outerRow]); outerColumn++ {
			if len(enhanced[outerRow][outerColumn]) != innerLength {
				panic("invalid inner row length")
			}
			for innerRow := 0; innerRow < len(enhanced[outerRow][outerColumn]); innerRow++ {
				if len(enhanced[outerRow][outerColumn][innerRow]) != innerLength {
					panic("invalid inner column length")
				}
				for innerColumn := 0; innerColumn < len(enhanced[outerRow][outerColumn][innerRow]); innerColumn++ {
					out[outerRow*innerLength+innerRow][outerColumn*innerLength+innerColumn] = enhanced[outerRow][outerColumn][innerRow][innerColumn]
				}
			}
		}
	}
	return out
}

func NewEmptyMatrix(dims int) Matrix {
	out := make(Matrix, dims)
	for i := range out {
		out[i] = make([]bool, dims)
	}
	return out
}

func (m Matrix) GetSubMatrices() (out [][]Matrix) {
	divisor := 2
	if len(m)%2 != 0 {
		divisor = 3
	}
	size := len(m)
	for row := 0; row < size/divisor; row++ {
		var l []Matrix
		for col := 0; col < size/divisor; col++ {
			l = append(l, m.GetMatrixAt(row*divisor, col*divisor, divisor))
		}
		out = append(out, l)
	}
	return out
}

func (m Matrix) GetMatrixAt(top, left, size int) Matrix {
	cp := m.Copy()
	var out Matrix
	for row := top; row < top+size; row++ {
		out = append(out, cp[row][left:left+size])
	}
	return out
}

func (m Matrix) Equal(b Matrix) bool {
	if &m == &b {
		return true
	}
	if len(m) != len(b) {
		return false
	}
	for i := 0; i < len(m); i++ {
		if len(m[i]) != len(b[i]) {
			return false
		}
		for j := 0; j < len(m[i]); j++ {
			if m[i][j] != b[i][j] {
				return false
			}
		}
	}
	return true
}
