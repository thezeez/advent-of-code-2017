package day21

import "testing"

func TestOnAfterEnhancements(t *testing.T) {
	tests := []struct {
		in           string
		enhancements string
		count        int
		out          int
	}{
		{".#./..#/###", "../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#", 2, 12},
	}

	for _, test := range tests {
		matrix := NewMatrix(test.in)
		enhancements := GetEnhancements(test.enhancements)
		actual := OnAfterEnhancements(matrix, enhancements, test.count)
		if actual != test.out {
			t.Errorf("OnAfterEnhancements(... %d) => %d, want %d", test.count, actual, test.out)
		}
	}
}
