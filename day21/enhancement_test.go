package day21

import (
	"reflect"
	"testing"
)

func TestGetEnhancement(t *testing.T) {
	tests := []struct {
		in  string
		out Enhancement
	}{
		{"../.# => ##./#../...",
			Enhancement{
				Size: 2,
				Output: Matrix{
					{true, true, false},
					{true, false, false},
					{false, false, false},
				},
				Inputs: []Matrix{
					{
						{false, false},
						{true, false},
					}, {
						{true, false},
						{false, false},
					},
					{
						{false, true},
						{false, false},
					}, {
						{false, false},
						{false, true},
					},
				},
			}},
	}
	for _, test := range tests {
		actual := GetEnhancement(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("GetEnhancement(%q) => %q, want %q", test.in, actual, test.out)
		}
	}
}

func TestEnhancement_Has(t *testing.T) {
	tests := []struct {
		in       string
		matrices []string
		valid    []bool
	}{
		{"###/#../.#. => ..../..../..../....",
			[]string{".#./..#/###", ".#./#../###", "#../#.#/##.", "###/..#/.#.", ".../.../...", ".#../..#./###./...."},
			[]bool{true, true, true, true, false, false}},
	}
	for _, test := range tests {
		e := GetEnhancement(test.in)
		for i, matrixIn := range test.matrices {
			m := NewMatrix(matrixIn)
			actual := e.Has(m)
			if actual != test.valid[i] {
				t.Errorf("%q.Has(%q) => %t, want %t", test.in, m, actual, test.valid[i])
			}
		}
	}
}
