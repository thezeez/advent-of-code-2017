package day10

import (
	"reflect"
	"testing"
)

func TestReverse(t *testing.T) {
	tests := []struct {
		list   []byte
		start  int
		length int
		out    []byte
	}{
		{
			[]byte{0, 1, 2, 3, 4},
			0,
			3,
			[]byte{2, 1, 0, 3, 4},
		},
		{
			[]byte{2, 1, 0, 3, 4},
			3,
			4,
			[]byte{4, 3, 0, 1, 2},
		},
		{
			[]byte{4, 3, 0, 1, 2},
			3,
			1,
			[]byte{4, 3, 0, 1, 2},
		},
		{
			[]byte{4, 3, 0, 1, 2},
			1,
			5,
			[]byte{3, 4, 2, 1, 0},
		},
	}

	for _, test := range tests {
		actual := make([]byte, len(test.list))
		copy(actual, test.list)

		reverse(actual, test.start, test.length)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("reverse(%v) => %v, want %v", test.list, actual, test.out)
		}
	}
}

func TestKnotHash(t *testing.T) {
	test := struct {
		size    int
		lengths string
		result  int
	}{
		5,
		"3, 4, 1, 5",
		12,
	}

	actual := KnotHash(test.lengths, test.size)
	if actual != test.result {
		t.Errorf("KnotHash(%q, %d) => %d, want %d", test.lengths, test.size, actual, test.result)
	}
}

func TestHash(t *testing.T) {
	tests := []struct {
		in  []byte
		out string
	}{
		{[]byte{65, 27, 9, 1, 4, 3, 40, 50, 91, 7, 6, 0, 2, 5, 68, 22}, "40"},
	}

	for _, test := range tests {
		actual := hash(test.in)
		if actual != test.out {
			t.Errorf("hash(%v) => %s, want %s", test.in, actual, test.out)
		}
	}
}

func TestMultiRoundKnotHash(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"", "a2582a3a0e66e6e86e3812dcb672a272"},
		{"AoC 2017", "33efeb34ea91902bb2f59c9920caa6cd"},
		{"1,2,3", "3efbe78a8d82f29979031a4aa0b16a9d"},
		{"1,2,4", "63960835bcdc130f0b66d7ff4f6a5a8e"},
	}

	for _, test := range tests {
		actual := MultiRoundKnotHash(test.in)
		if actual != test.out {
			t.Errorf("MultiRoundKnotHash(%q) => %s, want %s", test.in, actual, test.out)
		}
	}
}
