package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day10"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	fmt.Printf("KnotHash: %d\n", day10.KnotHash(input, 256))
	fmt.Printf("MultiRoundKnotHash: %s\n", day10.MultiRoundKnotHash(input))
}
