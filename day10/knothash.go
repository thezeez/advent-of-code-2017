package day10

import (
	"fmt"
	"strconv"
	"strings"
)

func KnotHash(input string, size int) int {
	lengths := getNumbers(input)
	list := makeList(size)
	var current, skip int

	for _, length := range lengths {
		reverse(list, current, length)
		current = (current + length + skip) % size
		skip++
	}

	return int(list[0]) * int(list[1])
}

func MultiRoundKnotHash(input string) string {
	size := 256
	lengths := []byte(strings.TrimSpace(input))
	lengths = append(lengths, 17, 31, 73, 47, 23)
	list := makeList(size)
	var current, skip int

	for round := 0; round < 64; round++ {
		for _, length := range lengths {
			reverse(list, current, int(length))
			current = (current + int(length) + skip) % size
			skip++
		}
	}

	return hash(list)
}

func makeList(length int) []byte {
	list := make([]byte, length)
	for i := 0; i < len(list); i++ {
		list[i] = byte(i)
	}
	return list
}

func getNumbers(input string) (numbers []int) {
	for _, field := range strings.Split(input, ",") {
		number, err := strconv.ParseInt(strings.TrimSpace(field), 10, 64)
		if err != nil {
			panic(err)
		}
		numbers = append(numbers, int(number))
	}
	return
}

func reverse(list []byte, pos, length int) {
	size := len(list)
	cp := make([]byte, size)
	copy(cp, list)
	for i := 0; i < length; i++ {
		list[(pos+length-i-1)%size] = cp[(pos+i)%size]
	}
}

func hash(list []byte) (result string) {
	if len(list)%16 != 0 {
		panic("hash: invalid input length")
	}
	for i := 0; i < len(list)/16; i++ {
		var block byte = 0
		for _, e := range list[i*16 : i*16+16] {
			block ^= e
		}
		result += fmt.Sprintf("%02x", block)
	}
	return
}
