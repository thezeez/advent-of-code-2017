package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day18"
	"bitbucket.org/thezeez/advent-of-code-2017/day23"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	instructions := day18.GetInstructions(input)

	fmt.Printf("Coprocessor: %d\n", day23.Coprocessor(instructions))
	fmt.Printf("NonDebugCoprocessor: %d\n", day23.NonDebugCoprocessor())
}
