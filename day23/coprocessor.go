package day23

import (
	"bitbucket.org/thezeez/advent-of-code-2017/day18"
)

func Coprocessor(instructions []day18.Instruction) int {
	var mulUsed int
	registers := make(map[string]int)

	for pos := 0; pos >= 0 && pos < len(instructions); pos++ {
		ins := instructions[pos]

		switch ins.Command {
		case "set":
			registers[ins.X.(string)] = day18.IntOrRegisterValue(ins.Y, registers)
		case "sub":
			registers[ins.X.(string)] -= day18.IntOrRegisterValue(ins.Y, registers)
		case "mul":
			registers[ins.X.(string)] *= day18.IntOrRegisterValue(ins.Y, registers)
			mulUsed++
		case "jnz":
			if day18.IntOrRegisterValue(ins.X, registers) != 0 {
				pos += day18.IntOrRegisterValue(ins.Y, registers) - 1
			}
		}
	}
	return mulUsed
}

func NonDebugCoprocessor() int {
	var b, c, d, f, h int

	b = 79*100 + 100000
	c = b + 17000

	for b <= c {
		f = 1
		d = 2

		for d = 2; d < b && f == 1; d++ {
			if b%d == 0 {
				f = 0
			}
		}
		if f == 0 {
			h += 1
		}
		b += 17
	}
	return h
	// Looks cooler than the input, right? Only took me an hour of being told what it actually does, so I totally cheated :P
	// The hint that it is very similar to day 18, which also did some modulo stuff, totally went over my head as well.
}
