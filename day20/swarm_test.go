package day20

import (
	"testing"
)

func TestClosest(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`
p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>
p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>`, 0},
	}

	for _, test := range tests {
		actual := Closest(GetParticles(test.in))
		if actual != test.out {
			t.Errorf("Closest(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCollision(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`
p=<-6,0,0>, v=< 3,0,0>, a=< 0,0,0>
p=<-4,0,0>, v=< 2,0,0>, a=< 0,0,0>
p=<-2,0,0>, v=< 1,0,0>, a=< 0,0,0>
p=< 3,0,0>, v=<-1,0,0>, a=< 0,0,0>`, 1},
	}

	for _, test := range tests {
		actual := Collision(GetParticles(test.in))
		if actual != test.out {
			t.Errorf("Collision(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
