package day20

import (
	"fmt"
	"regexp"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Coords struct {
	X int
	Y int
	Z int
}

func NewCoords(s []string) Coords {
	return Coords{
		X: helpers.IntOrPanic(s[0]),
		Y: helpers.IntOrPanic(s[1]),
		Z: helpers.IntOrPanic(s[2]),
	}
}

func (c *Coords) String() string {
	return fmt.Sprintf("<%d,%d,%d>", c.X, c.Y, c.Z)
}

type Particle struct {
	id           int
	position     Coords
	velocity     Coords
	acceleration Coords
}

func (p *Particle) String() string {
	return fmt.Sprintf("Particle{id=%d, p=%s, v=%s, a=%s}", p.id, p.position.String(), p.velocity.String(), p.acceleration.String())
}

func (p *Particle) Tick() {
	p.velocity.X += p.acceleration.X
	p.velocity.Y += p.acceleration.Y
	p.velocity.Z += p.acceleration.Z

	p.position.X += p.velocity.X
	p.position.Y += p.velocity.Y
	p.position.Z += p.velocity.Z
}

func (p *Particle) Distance(b Coords) int {
	return helpers.Abs(p.position.X-b.X) + helpers.Abs(p.position.Y-b.Y) + helpers.Abs(p.position.Z-b.Z)
}

var format = regexp.MustCompile(`([pva])=< ?(-?\d+), ?(-?\d+), ?(-?\d+)>`)

func GetParticles(input string) map[int]*Particle {
	id := 0
	particles := make(map[int]*Particle)
	for _, line := range strings.Split(input, "\n") {
		matches := format.FindAllStringSubmatch(line, -1)
		if len(matches) == 3 {
			particle := &Particle{id: id}
			for _, match := range matches {
				coords := NewCoords(match[2:])
				switch match[1] {
				case "p":
					particle.position = coords
				case "v":
					particle.velocity = coords
				case "a":
					particle.acceleration = coords
				}
			}
			particles[id] = particle
			id++
		}
	}
	return particles
}
