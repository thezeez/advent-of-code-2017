package day20

func Closest(particles map[int]*Particle) int {
	for i := 0; i < 1000; i++ {
		for _, p := range particles {
			p.Tick()
		}
	}

	var closest *Particle
	var closestDistance int
	for _, p := range particles {
		if closest == nil {
			closest = p
			closestDistance = p.Distance(Coords{0, 0, 0})
			continue
		}
		distance := p.Distance(Coords{0, 0, 0})
		if distance < closestDistance {
			closestDistance = distance
			closest = p
		}
	}
	return closest.id

}
func Collision(particles map[int]*Particle) int {
	for i := 0; i < 1000; i++ {
		var col []int
		for a, ba := range particles {
			for b, bb := range particles {
				if a == b {
					continue
				}
				if ba.Distance(bb.position) == 0 {
					col = append(col, a, b)
				}
			}
		}
		for _, id := range col {
			delete(particles, id)
		}

		for _, p := range particles {
			p.Tick()
		}
	}
	return len(particles)
}
