package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day20"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	fmt.Printf("Closest: %d\n", day20.Closest(day20.GetParticles(input)))
	fmt.Printf("Collision: %d\n", day20.Collision(day20.GetParticles(input)))
}
