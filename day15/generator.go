package day15

func Generator(done <-chan struct{}, state, factor, remainder, criteria int) <-chan int {
	out := make(chan int, 64)
	go func() {
		defer close(out)
		for {
			for acceptable := false; !acceptable; acceptable = state%criteria == 0 {
				state = (state * factor) % remainder
			}
			select {
			case out <- state:
				// Submit next acceptable value
			case <-done:
				// Judge is done
				return
			}
		}
	}()
	return out
}
