package day15

import "testing"

func TestJudge(t *testing.T) {
	tests := []struct {
		a          int
		b          int
		iterations int
		out        int
	}{
		{65, 8921, 5, 1},
		{65, 8921, 40000000, 588},
	}

	for _, test := range tests {
		actual := Judge(test.a, test.b, test.iterations)
		if actual != test.out {
			t.Errorf("Judge(%d, %d, %d) => %d, want %d", test.a, test.b, test.iterations, actual, test.out)
		}
	}
}

func TestJudgePicky(t *testing.T) {
	tests := []struct {
		a          int
		b          int
		iterations int
		out        int
		long       bool
	}{
		{65, 8921, 1056, 1, false},
		{65, 8921, 5000000, 309, true},
	}

	for _, test := range tests {
		if test.long && testing.Short() {
			t.Logf("skipped long test: JudgePicky(%d, %d, %d)", test.a, test.b, test.iterations)
			continue
		}
		actual := JudgePicky(test.a, test.b, test.iterations)
		if actual != test.out {
			t.Errorf("JudgePicky(%d, %d, %d) => %d, want %d", test.a, test.b, test.iterations, actual, test.out)
		}
	}
}

func benchJudge(b *testing.B, f func(int, int, int) int, iterations int) {
	for i := 0; i < b.N; i++ {
		f(783, 325, iterations)
	}
}

func BenchmarkJudge1000(b *testing.B)     { benchJudge(b, Judge, 1000) }
func BenchmarkJudge1000000(b *testing.B)  { benchJudge(b, Judge, 1000000) }
func BenchmarkJudge40000000(b *testing.B) { benchJudge(b, Judge, 40000000) }

func BenchmarkJudgePicky1000(b *testing.B)    { benchJudge(b, JudgePicky, 1000) }
func BenchmarkJudgePicky1000000(b *testing.B) { benchJudge(b, JudgePicky, 1000000) }
func BenchmarkJudgePicky5000000(b *testing.B) { benchJudge(b, JudgePicky, 5000000) }
