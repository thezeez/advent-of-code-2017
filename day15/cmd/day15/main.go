package main

import (
	"fmt"
	"regexp"

	"bitbucket.org/thezeez/advent-of-code-2017/day15"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	generators := getGenerators(input)

	fmt.Printf("Judge: %d\n", day15.Judge(generators["A"], generators["B"], 40000000))
	fmt.Printf("JudgePicky: %d\n", day15.JudgePicky(generators["A"], generators["B"], 5000000))
}

// Because why not
func getGenerators(input string) map[string]int {
	generators := make(map[string]int)
	generatorInput := regexp.MustCompile(`Generator ([AB]) starts with (\d+)`)

	matches := generatorInput.FindAllStringSubmatch(input, -1)
	for _, match := range matches {
		generators[match[1]] = helpers.IntOrPanic(match[2])
	}
	if generators["A"] == 0 {
		panic("generator A missing or zero")
	}
	if generators["B"] == 0 {
		panic("generator B missing or zero")
	}
	return generators
}
