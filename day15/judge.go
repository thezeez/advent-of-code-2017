package day15

const (
	FactorA   = 16807
	FactorB   = 48271
	Remainder = 2147483647
)

func Judge(stateA, stateB, iterations int) (matches int) {
	for i := 0; i < iterations; i++ {
		stateA = (stateA * FactorA) % Remainder
		stateB = (stateB * FactorB) % Remainder

		if stateA&0xFFFF == stateB&0xFFFF {
			matches++
		}
	}
	return
}

func JudgePicky(stateA, stateB, iterations int) (matches int) {
	done := make(chan struct{})
	defer close(done)

	aResult := Generator(done, stateA, FactorA, Remainder, 4)
	bResult := Generator(done, stateB, FactorB, Remainder, 8)

	for i := 0; i < iterations; i++ {
		if <-aResult&0xFFFF == <-bResult&0xFFFF {
			matches++
		}
	}
	return
}
