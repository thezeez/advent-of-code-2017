package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day02"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	fmt.Printf("MinMaxChecksum: %d\n", day02.MinMaxChecksum(input))
	fmt.Printf("DivisionChecksum: %d\n", day02.DivisionChecksum(input))
}
