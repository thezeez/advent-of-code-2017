package day02

import "testing"

func TestMinMaxChecksum(t *testing.T) {
	tests := []struct {
		in  string
		out int64
	}{
		{"5 1 9 5", 8},
		{"7 5 3", 4},
		{"2 4 6 8", 6},
	}

	for _, test := range tests {
		actual := MinMaxChecksum(test.in)
		if actual != test.out {
			t.Errorf("MinMaxChecksum(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestDivisionChecksum(t *testing.T) {
	tests := []struct {
		in  string
		out int64
	}{
		{"5 9 2 8", 4},
		{"9 4 7 3", 3},
		{"3 8 6 5", 2},
	}

	for _, test := range tests {
		actual := DivisionChecksum(test.in)
		if actual != test.out {
			t.Errorf("DivisionChecksum(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
