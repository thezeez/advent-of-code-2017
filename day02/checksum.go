package day02

import (
	"strconv"
	"strings"
)

func MinMaxChecksum(input string) (result int64) {
	numbers := getNumbers(input)
	for _, line := range numbers {
		min, max := findMinMax(line)
		result += max - min
	}
	return
}

func DivisionChecksum(input string) (result int64) {
	numbers := getNumbers(input)
	for _, line := range numbers {
		result += findDivision(line)
	}
	return
}

func getNumbers(input string) (result [][]int64) {
	for _, line := range strings.Split(input, "\n") {
		var lineNumbers []int64
		for _, numberString := range strings.Fields(line) {
			numberInt, err := strconv.ParseInt(numberString, 10, 64)
			if err != nil {
				panic(err)
			}
			lineNumbers = append(lineNumbers, numberInt)
		}
		if len(lineNumbers) > 0 {
			result = append(result, lineNumbers)
		}
	}
	return
}

func findMinMax(numbers []int64) (min, max int64) {
	for i, number := range numbers {
		if number < min || i == 0 {
			min = number
		}
		if number > max || i == 0 {
			max = number
		}
	}
	return
}

func findDivision(numbers []int64) (result int64) {
	if len(numbers) < 2 {
		return
	}
	for i, dividend := range numbers {
		for j, divisor := range numbers {
			if i != j && dividend%divisor == 0 {
				result = dividend / divisor
				return
			}
		}
	}
	return
}
