package day16

import (
	"reflect"
	"testing"
)

func TestGetDanceMoves(t *testing.T) {
	tests := []struct {
		in  string
		out []Performer
	}{
		{
			"s1,x3/14,pe/b",
			[]Performer{
				Spin{1},
				Exchange{3, 14},
				Partner{"e", "b"},
			},
		},
	}

	for _, test := range tests {
		actual := GetDanceMoves(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("GetDanceMoves(%q) =>\n%#v, want\n%#v", test.in, actual, test.out)
		}
	}
}

func TestSpin_Perform(t *testing.T) {
	tests := []struct {
		in   string
		size int
		out  string
	}{
		{"abcde", 3, "cdeab"},
		{"abcde", 1, "eabcd"},
		{"baedc", 1, "cbaed"},
	}

	for _, test := range tests {
		actual := Spin{test.size}.Perform(test.in)
		if actual != test.out {
			t.Errorf("Spin(%d).Perform(%q) => %q, want %q", test.size, test.in, actual, test.out)
		}
	}
}

func TestExchange_Perform(t *testing.T) {
	tests := []struct {
		in   string
		posA int
		posB int
		out  string
	}{
		{"eabcd", 3, 4, "eabdc"},
		{"cbaed", 3, 4, "cbade"},
		{"abcde", 0, 4, "ebcda"},
		{"abcde", 0, 1, "bacde"},
	}

	for _, test := range tests {
		actual := Exchange{test.posA, test.posB}.Perform(test.in)
		if actual != test.out {
			t.Errorf("Exchange(%d, %d).Perform(%q) => %q, want %q", test.posA, test.posB, test.in, actual, test.out)
		}
	}
}

func TestPartner_Perform(t *testing.T) {
	tests := []struct {
		in       string
		partnerA string
		partnerB string
		out      string
	}{
		{"eabdc", "e", "b", "baedc"},
		{"cbade", "e", "b", "ceadb"},
		{"abcde", "a", "e", "ebcda"},
	}

	for _, test := range tests {
		actual := Partner{test.partnerA, test.partnerB}.Perform(test.in)
		if actual != test.out {
			t.Errorf("Partner(%q, %q).Perform(%q) => %q, want %q", test.partnerA, test.partnerB, test.in, actual, test.out)
		}
	}
}
