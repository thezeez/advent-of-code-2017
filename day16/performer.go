package day16

import (
	"strings"
)

// Performer is an interface for a dance move.
type Performer interface {
	// Perform applies the dance move to a line of programs and returns their new order.
	Perform(string) string
}

// Spin shifts the last Count programs from the end to the front, maintaining their order.
type Spin struct {
	Count int
}

func (s Spin) Perform(state string) string {
	return state[len(state)-s.Count:] + state[:len(state)-s.Count]
}

// Exchange makes the programs at positions A and B swap places.
type Exchange struct {
	A int
	B int
}

func (e Exchange) Perform(state string) string {
	out := make([]byte, len(state))
	copy(out, state)
	out[e.A], out[e.B] = state[e.B], state[e.A]
	return string(out)
}

// Partner makes the programs named A and B swap places.
type Partner struct {
	A string
	B string
}

func (p Partner) Perform(state string) string {
	posA := strings.Index(state, p.A)
	posB := strings.Index(state, p.B)
	out := []byte(state)
	out[posA], out[posB] = out[posB], out[posA]
	return string(out)
}
