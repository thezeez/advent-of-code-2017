package day16

import (
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func GetDanceMoves(input string) (moves []Performer) {
	for _, move := range strings.Split(input, ",") {
		move := strings.TrimSpace(move)
		switch move[0] {
		case 's':
			moves = append(moves, Spin{helpers.IntOrPanic(move[1:])})
		case 'x':
			partners := strings.Split(move[1:], "/")
			aPos := helpers.IntOrPanic(partners[0])
			bPos := helpers.IntOrPanic(partners[1])
			moves = append(moves, Exchange{aPos, bPos})
		case 'p':
			moves = append(moves, Partner{string(move[1]), string(move[3])})
		}
	}
	return moves
}

func Dance(state string, moves []Performer) string {
	for _, move := range moves {
		state = move.Perform(state)
	}
	return state
}

func ABillionDances(state string, moves []Performer, repetitions int) string {
	initialState := state

	for i := 0; i < repetitions; i++ {
		state = Dance(state, moves)
		if state == initialState {
			repetitions = 1 + i + repetitions%(i+1)
		}
	}
	return state
}
