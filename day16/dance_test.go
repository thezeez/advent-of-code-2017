package day16

import "testing"

func TestDance(t *testing.T) {
	tests := []struct {
		in    string
		moves []Performer
		out   string
	}{
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			"baedc",
		},
		{
			"baedc",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			"ceadb",
		},
	}

	for _, test := range tests {
		actual := Dance(test.in, test.moves)
		if actual != test.out {
			t.Errorf("Dance(%q, %#v) => %q, want %q", test.in, test.moves, actual, test.out)
		}
	}
}

func TestABillionDances(t *testing.T) {
	// and a billion of them it is
	tests := []struct {
		in          string
		moves       []Performer
		repetitions int
		out         string
	}{
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			1,
			"baedc",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			2,
			"ceadb",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			3,
			"ecbda",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			4,
			"abcde",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			5,
			"baedc",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			1000000000,
			"abcde",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			1000000001,
			"baedc",
		},
		{
			"abcde",
			[]Performer{
				Spin{1},
				Exchange{3, 4},
				Partner{"e", "b"},
			},
			1000000004,
			"abcde",
		},
	}

	for _, test := range tests {
		actual := ABillionDances(test.in, test.moves, test.repetitions)
		if actual != test.out {
			t.Errorf("ABillionDances(%q, %#v, %d) => %q, want %q", test.in, test.moves, test.repetitions, actual, test.out)
		}
	}
}
