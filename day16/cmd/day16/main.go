package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day16"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	moves := day16.GetDanceMoves(input)

	initialState := "abcdefghijklmnop"

	fmt.Printf("Dance:          %q\n", day16.Dance(initialState, moves))
	fmt.Printf("ABillionDances: %q\n", day16.ABillionDances(initialState, moves, 1000000000))
}
