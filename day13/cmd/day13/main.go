package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day13"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	firewall := day13.New(input)

	severity, _ := day13.FirewallSeverity(firewall)
	fmt.Printf("FirewallSeverity: %d\n", severity)
	fmt.Printf("PacketDelay: %d\n", day13.PacketDelay(firewall))
}
