package day13

import (
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Firewall map[int]int

func FirewallSeverity(firewall Firewall) (severity int, caught bool) {
	for fDepth, fRange := range firewall {
		if Spotted(fRange, fDepth) {
			caught = true
			severity += fDepth * fRange
		}
	}
	return
}

func PacketDelay(firewall Firewall) (delay int) {
scan:
	for {
		for fDepth, sRange := range firewall {
			if Spotted(sRange, fDepth+delay) {
				delay++
				continue scan
			}
		}
		return
	}
}

func Spotted(sRange, delay int) bool {
	if sRange == 1 {
		return true
	}
	// Can have moved in one of two (2*) directions (sRange - 1) times
	return delay%(2*(sRange-1)) == 0
}

func New(input string) Firewall {
	firewall := make(Firewall)
	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		firewallData := strings.Split(line, ": ")
		if len(firewallData) != 2 {
			continue
		}
		fDepth, sRange := helpers.IntOrPanic(firewallData[0]), helpers.IntOrPanic(firewallData[1])
		firewall[fDepth] = sRange
	}
	return firewall
}
