package day13

import (
	"reflect"
	"testing"
)

func TestNew(t *testing.T) {
	tests := []struct {
		in  string
		out Firewall
	}{
		{
			"\n0: 3\n1: 2\n4: 4\n   6:    4  \n ",
			Firewall{0: 3, 1: 2, 4: 4, 6: 4},
		},
	}

	for _, test := range tests {
		actual := New(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("New(%q) => %v, want %v (in any order)", test.in, actual, test.out)
		}
	}
}

func TestSpotted(t *testing.T) {
	tests := []struct {
		sRange  int
		delay   int
		spotted bool
	}{
		{3, 0, true},
		{3, 1, false},
		{3, 2, false},
		{3, 3, false},
		{3, 4, true},
		{2, 0, true},
		{2, 1, false},
		{2, 2, true},
		{2, 3, false},
	}

	for _, test := range tests {
		actual := Spotted(test.sRange, test.delay)
		if actual != test.spotted {
			t.Errorf("Spotted(%d, %d) => %t, want %t", test.sRange, test.delay, actual, test.spotted)
		}
	}
}

func TestFirewallSeverity(t *testing.T) {
	tests := []struct {
		in       Firewall
		severity int
		caught   bool
	}{
		{Firewall{0: 3, 1: 2, 4: 4, 6: 4}, 24, true},
		{Firewall{1: 2, 2: 3, 3: 4, 4: 5}, 0, false},
	}

	for _, test := range tests {
		actual, caught := FirewallSeverity(test.in)
		if actual != test.severity {
			t.Errorf("FirewallSeverity(%v) => %d, %t want %d, %t", test.in, actual, caught, test.severity, test.caught)
		}
	}
}

func TestPacketDelay(t *testing.T) {
	tests := []struct {
		in  Firewall
		out int
	}{
		{Firewall{0: 3, 1: 2, 4: 4, 6: 4}, 10},
	}

	for _, test := range tests {
		actual := PacketDelay(test.in)
		if actual != test.out {
			t.Errorf("PacketDelay(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
