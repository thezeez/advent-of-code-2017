package main

import (
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/day05"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	instructions := getInstructions(input)
	fmt.Printf("Jump (Basic): %d\n", day05.Jump(instructions, day05.BasicOffset))
	fmt.Printf("Jump (Advanced): %d\n", day05.Jump(instructions, day05.AdvancedOffset))
}

func getInstructions(input string) (output []int) {
	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		num, err := strconv.ParseInt(line, 10, 64)
		if err != nil {
			panic(err)
		}
		output = append(output, int(num))
	}
	return
}
