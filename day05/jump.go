package day05

import (
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type offsetFunc func(int) int

func Jump(instructions []int, offset offsetFunc) (jumps int) {
	position := 0
	end := len(instructions)
	workingSet := helpers.Copy(instructions)
	for position >= 0 && position < end {
		position, workingSet[position] = position+workingSet[position], offset(workingSet[position])
		jumps++
	}
	return
}

func BasicOffset(instruction int) int {
	return instruction + 1
}

func AdvancedOffset(instruction int) int {
	if instruction >= 3 {
		return instruction - 1
	}
	return instruction + 1
}
