package day05

import (
	"testing"
)

func TestBasicOffset(t *testing.T) {

	tests := []struct {
		in  int
		out int
	}{
		{-3, -2},
		{-2, -1},
		{-1, 0},
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 4},
		{4, 5},
	}
	for _, test := range tests {
		actual := BasicOffset(test.in)
		if actual != test.out {
			t.Errorf("BasicOffset(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestAdvancedOffset(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{-3, -2},
		{-2, -1},
		{-1, 0},
		{0, 1},
		{1, 2},
		{2, 3},
		{3, 2},
		{4, 3},
	}
	for _, test := range tests {
		actual := AdvancedOffset(test.in)
		if actual != test.out {
			t.Errorf("AdvancedOffset(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestJumpBasic(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{0, 3, 0, 1, -3}, 5},
	}

	for _, test := range tests {
		actual := Jump(test.in, BasicOffset)
		if actual != test.out {
			t.Errorf("Jump(%v, BasicOffset) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestJumpAdvanced(t *testing.T) {
	tests := []struct {
		in  []int
		out int
	}{
		{[]int{0, 3, 0, 1, -3}, 10},
	}

	for _, test := range tests {
		actual := Jump(test.in, AdvancedOffset)
		if actual != test.out {
			t.Errorf("Jump(%v, AdvancedOffset) => %d, want %d", test.in, actual, test.out)
		}
	}
}
