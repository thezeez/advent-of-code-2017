package day07

import (
	"sort"
)

type Program struct {
	Name        string
	Weight      int
	totalWeight int
	parent      *Program
	children    []*Program
}

func (p *Program) CalculateWeights() {
	p.totalWeight = p.Weight
	for _, child := range p.children {
		child.CalculateWeights()
		p.totalWeight += child.totalWeight
	}
}

func (p *Program) Root() *Program {
	if p.parent == nil {
		return p
	}
	return p.parent.Root()
}

type weightData struct {
	weight int
	count  int
	child  *Program
}

type weightMap []weightData

func (m weightMap) Len() int           { return len(m) }
func (m weightMap) Less(i, j int) bool { return m[i].count < m[j].count }
func (m weightMap) Swap(i, j int)      { m[i], m[j] = m[j], m[i] }

func (p *Program) UnbalancedChild() (*Program, int) {
	weights := make(weightMap, 1)
children:
	for i, child := range p.children {
		if i == 0 {
			weights[0].count++
			weights[0].weight = child.totalWeight
			weights[0].child = child
			continue children
		}
		for w := range weights {
			if weights[w].weight == child.totalWeight {
				weights[w].count++
				continue children
			}
		}
		weights = append(weights, weightData{child.totalWeight, 1, child})
	}

	if len(weights) <= 1 {
		return nil, 0
	}

	sort.Sort(weights)
	unbalancedChild, weightDifference := weights[0].child.UnbalancedChild()
	if unbalancedChild == nil {
		return weights[0].child, weights[0].weight - weights[1].weight
	}
	return unbalancedChild, weightDifference
}
