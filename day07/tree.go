package day07

import (
	"regexp"

	"strconv"
	"strings"
)

var programData = regexp.MustCompile(`([a-z]+) \((-?\d+)\)(?: -> )?|([a-z]+),? ?`)

func BuildTree(input string) *Program {
	programs := make(map[string]*Program)
	childPrograms := make(map[string][]string)

	for _, line := range strings.Split(input, "\n") {
		program, children := getProgram(line)
		if program == nil || program.Name == "" {
			continue
		}
		programs[program.Name] = program
		childPrograms[program.Name] = children
	}
	for parent, children := range childPrograms {
		if val, ok := programs[parent]; ok {
			for _, child := range children {
				val.children = append(val.children, programs[child])
				programs[child].parent = val
			}
		}
	}
	for _, program := range programs {
		root := program.Root()
		if root != nil {
			root.CalculateWeights()
			return root
		}
	}
	return nil
}

func getProgram(input string) (output *Program, children []string) {
	match := programData.FindAllStringSubmatch(input, -1)
	if match != nil {
		weight, err := strconv.ParseInt(match[0][2], 10, 64)
		if err != nil {
			panic(err)
		}
		output = &Program{Name: match[0][1], Weight: int(weight)}
		for i := 1; i < len(match); i++ {
			if match[i][3] != "" {
				children = append(children, match[i][3])
			}
		}
	}
	return
}
