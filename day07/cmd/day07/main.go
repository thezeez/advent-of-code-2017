package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day07"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	root := day07.BuildTree(input)
	if root == nil {
		fmt.Println("No tree found")
		return
	}

	fmt.Printf("Root: %q\n", root.Name)

	unbalanced, difference := root.UnbalancedChild()
	fmt.Printf("Balance: %q should weigh %d\n", unbalanced.Name, unbalanced.Weight-difference)

}
