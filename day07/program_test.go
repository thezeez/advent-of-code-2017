package day07

import "testing"

func TestProgram_UnbalancedChild(t *testing.T) {
	tests := []struct {
		input      string
		name       string
		difference int
	}{
		{
			"pbga (66)\n		xhth (57)\n		ebii (61)\n		havc (66)\n		ktlj (57)\n		fwft (72) -> ktlj, cntj, xhth\n		qoyq (66)\n		padx (45) -> pbga, havc, qoyq\n		tknk (41) -> ugml, padx, fwft\n		jptl (61)\n		ugml (68) -> gyxo, ebii, jptl\n		gyxo (61)\n		cntj (57)",
			"ugml",
			8,
		},
		{
			"a (1) -> b, c, d\nb (2)\nc (1)\nd (1)",
			"b",
			1,
		},
		{
			"a (1) -> b, c, d\nb (2)\nc (-2)\nd (2)",
			"c",
			-4,
		},
	}

	for _, test := range tests {
		tree := BuildTree(test.input)
		unbalanced, difference := tree.UnbalancedChild()
		if unbalanced == nil {
			t.Fatalf("Program.UnbalancedChild() => nil, want %q, %d", test.name, test.difference)
		}
		if unbalanced.Name != test.name || difference != test.difference {
			t.Errorf("Program.UnbalancedChild() => %q, %d; want %q, %d", unbalanced.Name, difference, test.name, test.difference)
		}
	}
}
