package day07

import (
	"reflect"
	"testing"
)

func TestGetDisc(t *testing.T) {
	tests := []struct {
		in       string
		out      *Program
		children []string
	}{
		{"pbga (66)", &Program{Name: "pbga", Weight: 66}, nil},
		{"fwft (72) -> ktlj, cntj, xhth", &Program{Name: "fwft", Weight: 72}, []string{"ktlj", "cntj", "xhth"}},
	}

	for _, test := range tests {
		actual, actualChildren := getProgram(test.in)
		if !reflect.DeepEqual(actual, test.out) || !reflect.DeepEqual(test.children, actualChildren) {
			t.Errorf("getProgram(%q) => %+v %q, want %+v %q", test.in, actual, actualChildren, test.out, test.children)
		}
	}
}

func TestBuildTree(t *testing.T) {
	test := struct {
		in  string
		out string
	}{
		"pbga (66)\n		xhth (57)\n		ebii (61)\n		havc (66)\n		ktlj (57)\n		fwft (72) -> ktlj, cntj, xhth\n		qoyq (66)\n		padx (45) -> pbga, havc, qoyq\n		tknk (41) -> ugml, padx, fwft\n		jptl (61)\n		ugml (68) -> gyxo, ebii, jptl\n		gyxo (61)\n		cntj (57)",
		"tknk",
	}

	root := BuildTree(test.in)
	if root == nil {
		t.Errorf("BuildTree(...) => root nil, want %q", test.out)
	} else if root.Name != test.out {
		t.Errorf("BuildTree(...) => root %q, want %q", root.Name, test.out)
	}
}
