package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day22"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	grid, center := day22.GetMap(input)
	fmt.Printf("Infect: %d\n", day22.Infect(grid, center, 10000))

	advancedGrid, center := day22.GetAdvancedMap(input)
	fmt.Printf("InfectEvolved: %d\n", day22.InfectEvolved(advancedGrid, center, 10000000))
}
