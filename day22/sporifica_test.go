package day22

import "testing"

func TestInfect(t *testing.T) {
	tests := []struct {
		in     string
		bursts int
		out    int
	}{
		{"..#\n#..\n...", 1, 1},
		{"..#\n#..\n...", 2, 1},
		{"..#\n#..\n...", 3, 2},
		{"..#\n#..\n...", 4, 3},
		{"..#\n#..\n...", 5, 4},
		{"..#\n#..\n...", 6, 5},
		{"..#\n#..\n...", 7, 5},
		{"..#\n#..\n...", 70, 41},
		{"..#\n#..\n...", 10000, 5587},
	}

	for _, test := range tests {
		grid, center := GetMap(test.in)
		actual := Infect(grid, center, test.bursts)
		if actual != test.out {
			t.Errorf("Infect(%q, %v, %d) => %d, want %d", test.in, center, test.bursts, actual, test.out)
		}
	}
}

func TestInfectEvolved(t *testing.T) {
	tests := []struct {
		in     string
		bursts int
		out    int
	}{
		{"..#\n#..\n...\n", 1, 0},
		{"..#\n#..\n...\n", 2, 0},
		{"..#\n#..\n...\n", 3, 0},
		{"..#\n#..\n...\n", 4, 0},
		{"..#\n#..\n...\n", 5, 0},
		{"..#\n#..\n...\n", 6, 0},
		{"..#\n#..\n...\n", 7, 1},
		{"..#\n#..\n...\n", 100, 26},
		{"..#\n#..\n...\n", 10000000, 2511944},
	}

	for _, test := range tests {
		grid, center := GetAdvancedMap(test.in)
		actual := InfectEvolved(grid, center, test.bursts)
		if actual != test.out {
			t.Errorf("InfectEvolved(%q, %v, %d) => %d, want %d", test.in, center, test.bursts, actual, test.out)
		}
	}
}
