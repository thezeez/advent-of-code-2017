package day22

import (
	"strings"
)

func Infect(grid map[Coord]bool, center Coord, bursts int) int {
	virus := Virus{Position: center}
	for i := 0; i < bursts; i++ {
		virus.Burst(grid)
	}
	return virus.Infections
}

func InfectEvolved(grid map[Coord]int, center Coord, bursts int) int {
	virus := EvolvedVirus{Position: center}
	for i := 0; i < bursts; i++ {
		virus.Burst(grid)
	}
	return virus.Infections
}

func GetMap(input string) (map[Coord]bool, Coord) {
	input = strings.TrimSpace(input)
	out := make(map[Coord]bool)
	rows := strings.Split(input, "\n")
	center := Coord{X: len(rows) / 2}
	for y, row := range rows {
		row = strings.TrimSpace(row)
		if y == 0 {
			center.Y = len(row) / 2
		}
		for x, col := range row {
			out[Coord{x, y}] = col == '#'
		}
	}
	return out, center
}

func GetAdvancedMap(input string) (map[Coord]int, Coord) {
	input = strings.TrimSpace(input)
	out := make(map[Coord]int)
	rows := strings.Split(input, "\n")
	center := Coord{X: len(rows) / 2}
	for y, row := range rows {
		row = strings.TrimSpace(row)
		if y == 0 {
			center.Y = len(row) / 2
		}
		for x, col := range row {
			if col == '#' {

				out[Coord{x, y}] = 2
			}
		}
	}
	return out, center
}
