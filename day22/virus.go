package day22

type Virus struct {
	Heading    int
	Position   Coord
	Infections int
}

type Coord struct {
	X int
	Y int
}

func (c Coord) Add(b Coord) Coord {
	return Coord{c.X + b.X, c.Y + b.Y}
}

var directions = []Coord{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func (v *Virus) Burst(grid map[Coord]bool) {
	if grid[v.Position] {
		v.Turn(1)
	} else {
		v.Turn(-1)
		v.Infections++
	}
	grid[v.Position] = !grid[v.Position]
	v.Position = v.Position.Add(directions[v.Heading])
}

func (v *Virus) Turn(direction int) {
	v.Heading = (v.Heading + direction) % 4
	if v.Heading < 0 {
		v.Heading += 4
	}
}

type EvolvedVirus struct {
	Heading    int
	Position   Coord
	Infections int
}

func (v *EvolvedVirus) Burst(grid map[Coord]int) {
	switch grid[v.Position] {
	case 0:
		v.Turn(-1)
	case 1:
		v.Infections++
	case 2:
		v.Turn(1)
	case 3:
		v.Turn(2)
	}
	grid[v.Position] = (grid[v.Position] + 1) % 4
	v.Position = v.Position.Add(directions[v.Heading])
}

func (v *EvolvedVirus) Turn(direction int) {
	v.Heading = (v.Heading + direction) % 4
	if v.Heading < 0 {
		v.Heading += 4
	}
}
