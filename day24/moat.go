package day24

func Strongest(components []Component) int {
	root := &Bridge{}
	buildBridge(components, root)
	return root.highestStrength()
}

func Longest(components []Component) (int, int) {
	root := &Bridge{}
	buildBridge(components, root)
	root.keepLongestLength()
	return root.longestLength() - 1, root.highestStrength()
}
