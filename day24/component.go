package day24

import (
	"regexp"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Component struct {
	Ports []int
}

func GetComponents(input string) (components []Component) {
	matcher := regexp.MustCompile(`(\d+)/(\d+)`)
	for _, match := range matcher.FindAllStringSubmatch(input, -1) {
		portA := helpers.IntOrPanic(match[1])
		portB := helpers.IntOrPanic(match[2])
		components = append(components, Component{[]int{portA, portB}})
	}
	return
}

func getComponentsForPort(port int, components []Component) (fitting []int) {
	for i, c := range components {
		for _, p := range c.Ports {
			if p == port {
				fitting = append(fitting, i)
				break
			}
		}
	}
	return
}
