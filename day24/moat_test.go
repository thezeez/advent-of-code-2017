package day24

import "testing"

func TestStrongest(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"0/2 2/2 2/3 3/4 3/5 0/1 10/1 9/10", 31},
	}

	for _, test := range tests {
		components := GetComponents(test.in)
		actual := Strongest(components)
		if actual != test.out {
			t.Errorf("Strongest(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestLongest(t *testing.T) {
	tests := []struct {
		in       string
		length   int
		strength int
	}{
		{"0/2 2/2 2/3 3/4 3/5 0/1 10/1 9/10", 4, 19},
	}

	for _, test := range tests {
		components := GetComponents(test.in)
		length, strength := Longest(components)
		if length != test.length {
			t.Errorf("Longest(%q) => %d, %d, want %d, %d", test.in, length, strength, test.length, test.strength)
		}
	}
}
