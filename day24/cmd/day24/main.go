package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day24"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	components := day24.GetComponents(input)

	fmt.Printf("Strongest: %d\n", day24.Strongest(components))
	length, strength := day24.Longest(components)
	fmt.Printf("Longest: %d with strength %d\n", length, strength)
}
