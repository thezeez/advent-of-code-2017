package day24

import (
	"fmt"
	"strings"
)

type Bridge struct {
	parent   *Bridge
	children []*Bridge
	inPort   int
	outPort  int
}

func buildBridge(components []Component, parent *Bridge) {
	indices := getComponentsForPort(parent.outPort, components)
	for _, i := range indices {
		child := &Bridge{
			parent: parent,
			inPort: parent.outPort,
		}
		if components[i].Ports[0] == parent.outPort {
			child.outPort = components[i].Ports[1]
		} else {
			child.outPort = components[i].Ports[0]
		}
		parent.children = append(parent.children, child)

		bridgeComponents := make([]Component, len(components))
		copy(bridgeComponents, components)
		bridgeComponents = append(bridgeComponents[:i], bridgeComponents[i+1:]...)
		buildBridge(bridgeComponents, child)
	}
}

func printBridge(bridge *Bridge, level int) {
	fmt.Printf("%s%2d - %2d\n", strings.Repeat(" ", level), bridge.inPort, bridge.outPort)
	for _, child := range bridge.children {
		printBridge(child, level+5)
	}
}

func (b *Bridge) highestStrength() int {
	var highest int
	for i, child := range b.children {
		if i == 0 {
			highest = child.highestStrength()
			continue
		}
		strength := child.highestStrength()
		if strength > highest {
			highest = strength
		}
	}
	return b.inPort + b.outPort + highest
}

func (b *Bridge) longestLength() int {
	var longest int
	for i, child := range b.children {
		if i == 0 {
			longest = child.longestLength()
			continue
		}
		length := child.longestLength()
		if length > longest {
			longest = length
		}
	}
	return 1 + longest
}

func (b *Bridge) keepLongestLength() {
	longest := b.longestLength()
	var keep []*Bridge
	for _, child := range b.children {
		child.keepLongestLength()
		if child.longestLength() == longest-1 {
			keep = append(keep, child)
		}
	}
	b.children = keep
}
