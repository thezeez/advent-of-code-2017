package day12

import "testing"

func TestZeroGroup(t *testing.T) {
	tests := []struct {
		in  map[int][]int
		out int
	}{
		{map[int][]int{
			0: {2},
			1: {1},
			2: {0, 3, 4},
			3: {2, 4},
			4: {2, 3, 6},
			5: {6},
			6: {4, 5},
		}, 6},
		{map[int][]int{
			2: {1},
			1: {0, 2},
			0: {1},
		}, 3},
	}

	for _, test := range tests {
		actual := ZeroGroup(test.in)
		if actual != test.out {
			t.Errorf("ZeroGroup(%d) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestTotalGroups(t *testing.T) {
	tests := []struct {
		in  map[int][]int
		out int
	}{
		{map[int][]int{
			0: {2},
			1: {1},
			2: {0, 3, 4},
			3: {2, 4},
			4: {2, 3, 6},
			5: {6},
			6: {4, 5},
		}, 2},
		{map[int][]int{
			0: {0},
			1: {1},
			2: {3},
			3: {2},
		}, 3},
	}

	for _, test := range tests {
		actual := TotalGroups(test.in)
		if actual != test.out {
			t.Errorf("TotalGroups(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
