package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day12"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	programs := day12.ParsePrograms(input)

	fmt.Printf("ZeroGroup: %d\n", day12.ZeroGroup(programs))
	fmt.Printf("TotalGroups: %d\n", day12.TotalGroups(programs))
}
