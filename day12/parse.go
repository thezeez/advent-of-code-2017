package day12

import (
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func ParsePrograms(input string) map[int][]int {
	programs := make(map[int][]int)
	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		program, connections := ParseProgram(line)
		if program < 0 {
			continue
		}
		programs[program] = connections
	}
	return programs
}

func ParseProgram(input string) (int, []int) {
	var programData = regexp.MustCompile(`(\d+) <-> |(\d+),? ?`)
	match := programData.FindAllStringSubmatch(input, -1)
	if match != nil {
		source, err := strconv.ParseInt(match[0][1], 10, 64)
		if err != nil {
			panic(err)
		}
		program := int(source)
		connections := make([]int, len(match)-1)
		for i := 1; i < len(match); i++ {
			connections[i-1] = helpers.IntOrPanic(match[i][2])
		}
		return program, connections
	}
	return -1, nil
}
