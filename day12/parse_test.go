package day12

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func TestParseProgram(t *testing.T) {
	tests := []struct {
		in          string
		id          int
		connections []int
	}{
		{"0 <-> 2", 0, []int{2}},
		{"1 <-> 1", 1, []int{1}},
		{"2 <-> 0, 3, 4", 2, []int{0, 3, 4}},
		{"3 <-> 2, 4", 3, []int{2, 4}},
		{"4 <-> 2, 3, 6", 4, []int{2, 3, 6}},
		{"5 <-> 6", 5, []int{6}},
		{"\n    6 <->   4,   5   \n ", 6, []int{4, 5}},
	}

	for _, test := range tests {
		program, connections := ParseProgram(test.in)
		if program != test.id || !helpers.Equal(connections, test.connections) {
			t.Errorf("ParseProgram(%q) => %d, %d, want %d, %d", test.in, program, connections, test.id, test.connections)
		}
	}
}
