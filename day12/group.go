package day12

import "bitbucket.org/thezeez/advent-of-code-2017/helpers"

func ZeroGroup(programs map[int][]int) int {
	var current int

	seen := []int{0}
	toVisit := []int{0}

	for len(toVisit) > 0 {
		current, toVisit = toVisit[0], toVisit[1:]

		for _, connection := range programs[current] {
			if !helpers.Contains(seen, connection) {
				toVisit = append(toVisit, connection)
				seen = append(seen, connection)
			}
		}
	}
	return len(seen)
}

func TotalGroups(programs map[int][]int) int {
	var current int
	var seen []int
	var groups int

	for root, children := range programs {
		if helpers.Contains(seen, root) {
			continue
		}
		groups++

		toVisit := children

		for len(toVisit) > 0 {
			current, toVisit = toVisit[0], toVisit[1:]

			for _, connection := range programs[current] {
				if !helpers.Contains(seen, connection) {
					toVisit = append(toVisit, connection)
					seen = append(seen, connection)
				}
			}
		}
	}
	return groups
}
