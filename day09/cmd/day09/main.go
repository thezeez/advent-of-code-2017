package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day09"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")

	stream := day09.New(input)

	fmt.Printf("Score: %d\n", stream.Score())
	fmt.Printf("Garbage: %d\n", stream.Garbage)
}
