package day09

import "testing"

func TestStream_deleteIgnored(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"<{!>}>", "<{}>"},
		{"<!!>", "<>"},
		{"<!!!>>", "<>"},
		{`<{o"i!a,<{i<a>`, `<{o"i,<{i<a>`},
	}

	for _, test := range tests {
		stream := Stream{Data: test.in}
		stream.deleteIgnored()
		if stream.Data != test.out {
			t.Errorf("Stream{%q}.deleteIgnored() => %q, want %q", test.in, stream.Data, test.out)
		}
	}
}

func TestStream_deleteGarbage(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"1<>1", "11"},
		{"2<random characters>2", "22"},
		{"3<<<<>3", "33"},
		{"4<{}>4", "44"},
		{`5<{o"i!a,<{i<a>5`, "55"},
		{"{<a>,<a>,<a>,<a>}", "{,,,}"},
		{"{{<a>},{<a>},{<a>},{<a>}}", "{{},{},{},{}}"},
	}

	for _, test := range tests {
		stream := Stream{Data: test.in}
		stream.deleteGarbage()
		if stream.Data != test.out {
			t.Errorf("Stream{%q}.deleteGarbage() => %q, want %q", test.in, stream.Data, test.out)
		}
	}
}

func TestStream_Score(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"{}", 1},
		{"{{{}}}", 6},
		{"{{},{}}", 5},
		{"{{{},{},{{}}}}", 16},
		{"{<a>,<a>,<a>,<a>}", 1},
		{"{{<ab>},{<ab>},{<ab>},{<ab>}}", 9},
		{"{{<!!>},{<!!>},{<!!>},{<!!>}}", 9},
		{"{{<a!>},{<a!>},{<a!>},{<ab>}}", 3},
	}

	for _, test := range tests {
		actual := New(test.in).Score()
		if actual != test.out {
			t.Errorf("Stream{%q}.Score() => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestStreamGarbageValue(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"<>", 0},
		{"<random characters>", 17},
		{"<<<<>", 3},
		{"<{!>}>", 2},
		{"<!!>", 0},
		{"<!!!>>", 0},
		{`<{o"i!a,<{i<a>`, 10},
	}

	for _, test := range tests {
		actual := New(test.in).Garbage
		if actual != test.out {
			t.Errorf("Stream{%q}.Garbage => %d, want %d", test.in, actual, test.out)
		}
	}
}
