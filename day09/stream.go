package day09

import (
	"regexp"
)

type Stream struct {
	Data    string
	Garbage int
}

// Turns out there is no negative lookahead...
var ignore = regexp.MustCompile(`!.`)
var garbage = regexp.MustCompile(`<.*?>`)

func New(input string) *Stream {
	s := &Stream{Data: input}
	s.deleteIgnored()
	s.deleteGarbage()
	return s
}

func (s *Stream) deleteIgnored() {
	s.Data = ignore.ReplaceAllString(s.Data, "")
}

func (s *Stream) deleteGarbage() {
	s.Data = garbage.ReplaceAllStringFunc(s.Data, func(input string) string {
		s.Garbage += len(input) - 2
		return ""
	})
}

func (s *Stream) Score() (score int) {
	var cur int
	for _, char := range s.Data {
		switch char {
		case '{':
			cur++
			break
		case '}':
			score += cur
			cur--
			break
		}
	}
	return
}
