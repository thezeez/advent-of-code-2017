BASE_PATH="${GOPATH}/src/bitbucket.org/${BITBUCKET_REPO_OWNER}"
mkdir -pv "${BASE_PATH}"
export REPORTS_PATH="${PWD}/test-reports"
mkdir -pv "${REPORTS_PATH}"
export IMPORT_PATH="${BASE_PATH}/${BITBUCKET_REPO_SLUG}"
ln -s ${PWD} ${IMPORT_PATH}
cd $IMPORT_PATH
go get -v github.com/tebeka/go2xunit
