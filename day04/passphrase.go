package day04

import (
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type validationFunc func(string) bool

func HighEntropyPassphrase(input string, validator validationFunc) (valid int) {
	for _, line := range strings.Split(input, "\n") {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}
		if validator(line) {
			valid += 1
		}
	}
	return
}

var nothing struct{}

type wordMap map[string]struct{}

func ValidatePassphrase(passphrase string) bool {
	words := make(wordMap)
	for _, word := range strings.Fields(passphrase) {
		if _, exists := words[word]; exists {
			return false
		}
		words[word] = nothing
	}
	return true
}

func ValidateNoAnagramPassphrase(passphrase string) bool {
	words := make(wordMap)
	for _, word := range strings.Fields(passphrase) {
		sorted := helpers.SortString(word)
		if _, exists := words[sorted]; exists {
			return false
		}
		words[sorted] = nothing
	}
	return true
}
