package day04

import (
	"testing"
)

func TestValidatePassphrase(t *testing.T) {
	tests := []struct {
		in    string
		valid bool
	}{
		{"aa bb cc dd", true},
		{"aa bb cc dd aa", false},
		{"aa bb cc dd aaa", true},
	}

	for _, test := range tests {
		actual := ValidatePassphrase(test.in)
		if actual != test.valid {
			t.Errorf("ValidatePassphrase(%q) => %t, want %t", test.in, actual, test.valid)
		}
	}
}

func TestValidateNoAnagramPassphrase(t *testing.T) {
	tests := []struct {
		in    string
		valid bool
	}{
		{"abcde fghij", true},
		{"abcde xyz ecdab", false},
		{"a ab abc abd abf abj", true},
		{"iiii oiii ooii oooi oooo", true},
		{"oiii ioii iioi iiio", false},
	}

	for _, test := range tests {
		actual := ValidateNoAnagramPassphrase(test.in)
		if actual != test.valid {
			t.Errorf("ValidateNoAnagramPassphrase(%q) => %t, want %t", test.in, actual, test.valid)
		}
	}
}

func BenchmarkValidatePassphrase(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ValidatePassphrase("qddpdk trfxpip pnsowj hidgvnf prur rsrautp aamykfm fysqjmq xwzjane mbmtxhf oqctt rtnyxo qixfd nphekk mouzk gny fpzquw qgywx rpr gqydze")
	}
}

func BenchmarkValidateNoAnagramPassphrase(b *testing.B) {
	for n := 0; n < b.N; n++ {
		ValidateNoAnagramPassphrase("qddpdk trfxpip pnsowj hidgvnf prur rsrautp aamykfm fysqjmq xwzjane mbmtxhf oqctt rtnyxo qixfd nphekk mouzk gny fpzquw qgywx rpr gqydze")
	}
}
