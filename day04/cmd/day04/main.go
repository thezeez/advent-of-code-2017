package main

import (
	"fmt"

	"bitbucket.org/thezeez/advent-of-code-2017/day04"
	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func main() {
	input := helpers.ReadStringFromFile("input.txt")
	fmt.Printf("HighEntropyPassphrase: %d\n", day04.HighEntropyPassphrase(input, day04.ValidatePassphrase))
	fmt.Printf("NoAnagramPassphrase: %d\n", day04.HighEntropyPassphrase(input, day04.ValidateNoAnagramPassphrase))
}
